package in.tvac.akshaye.lapichat.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.model.api.BlinkTimeLineResponse;
import in.tvac.akshaye.lapichat.interfaces.LoadItems;
import in.tvac.akshaye.lapichat.views.showmore.ReadMoreTextView;

import java.util.*;

public class TimeLineAdapter extends ListAdapter<BlinkTimeLineResponse, RecyclerView.ViewHolder> {


    private Timer timer;
    private int mCounter = 0;
    private boolean isUpCalled = true;
    private Dialog dialog;
    private ProgressBar progressBarHorizontal;

   /* private ReadMoreTextView.ResultCallback callback = new ReadMoreTextView.ResultCallback() {
        @Override
        public void onResult(boolean isMore, boolean showExpanded, int index, int position, CharSequence collapseText, CharSequence expandedText) {
            if (localData.size() > position) {
                ReadMoreData data = new ReadMoreData();
                data.setDataSet(true);
                data.setLineEndIndex(index);
                data.setReadMore(isMore);
                data.setShowTrimExpandedText(showExpanded);
                data.setTrimCollapsedText(collapseText);
                data.setTrimExpandedText(expandedText);
                localData.set(position, data);
            }
        }
    };*/

    enum TYPE {
        IMAGE,
        VIDEO
    }

    ArrayList<BlinkTimeLineResponse> mTimeLineList;
    //ArrayList<ReadMoreData> localData = new ArrayList<>();

    //private HomeFragment blinktimelineFragment;
    private LoadItems mLoadItems;

    public TimeLineAdapter(ArrayList<BlinkTimeLineResponse> list, LoadItems loadItems) {
        super(DIFF_CALLBACK);
        this.mTimeLineList = list;
        this.mLoadItems = loadItems;
        /*localData = new ArrayList<>();
        localData.clear();
        for (int i = 0; i < mTimeLineList.size(); i++) {
            localData.add(new ReadMoreData());
        }*/
    }


    static final DiffUtil.ItemCallback<BlinkTimeLineResponse> DIFF_CALLBACK = new DiffUtil.ItemCallback<BlinkTimeLineResponse>() {
        @Override
        public boolean areItemsTheSame(@NonNull BlinkTimeLineResponse oldUser, @NonNull BlinkTimeLineResponse newUser) {
            // User properties may have changed if reloaded from the DB, but ID is fixed
            return oldUser.getId().equals(newUser.getId());
        }

        @Override
        public boolean areContentsTheSame(
                @NonNull BlinkTimeLineResponse oldUser, @NonNull BlinkTimeLineResponse newUser) {
            // NOTE: if you use equals, your object must properly override Object#equals()
            // Incorrectly returning false here will result in too many animations.
            return oldUser.equals(newUser);
        }
    };


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case 0:
                //ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_layout, parent, false);
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_layout, parent, false);
                return new ImageHolder(view);
            case 1:
                //ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_layout, parent, false);
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video_layout, parent, false);
                return new VideoHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d("onBindViewHolder", "---------------------------------------" + position);
        String userName = mTimeLineList.get(position).getUsername();
        if (holder instanceof VideoHolder) {
            String caption = mTimeLineList.get(position).getCaption();

            if (!TextUtils.isEmpty(userName)) {
                ((VideoHolder) holder).mUsernameTextView.setText(userName);
            }

           /* if (localData.size() > position && localData.get(position).isDataSet()) {
                ((VideoHolder) holder).mCaptionTextView.updateExistingData(localData.get(position), position);
            }*/
            if (caption != null && caption.trim().length() > 0) {
                ((VideoHolder) holder).mCaptionTextView.setText(caption);
                ((VideoHolder) holder).mCaptionTextView.setVisibility(View.VISIBLE);
            } else {
                ((VideoHolder) holder).mCaptionTextView.setText(" ");
                //((VideoHolder) holder).mCaptionTextView.setVisibility(View.GONE);
            }
            //((VideoHolder) holder).mCaptionTextView.setCallback(callback, position);

            ((VideoHolder) holder).bind(Uri.parse(mTimeLineList.get(position).getVideo_url()));
            String image = mTimeLineList.get(position).getImage();
            String postImage = mTimeLineList.get(position).getPostImage();
            String thumbUrl = "";
            if (image != null) {
                thumbUrl = image;
            } else if (postImage != null) {
                thumbUrl = postImage;
            }
            Glide.with(holder.itemView.getContext())
                    .asBitmap().load(thumbUrl)
                    .listener(new RequestListener<Bitmap>() {
                                  @Override
                                  public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                      //Toast.makeText(cxt,getResources().getString(R.string.unexpected_error_occurred_try_again),Toast.LENGTH_SHORT).show();
                                      return false;
                                  }

                                  @Override
                                  public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                                      ((VideoHolder) holder).videoThumbImage.setImageBitmap(resource);
                                      return false;
                                  }

                              }
                    ).submit();
        } else if (holder instanceof ImageHolder) {
            /*if (localData.size() > position && localData.get(position).isDataSet()) {
                ((ImageHolder) holder).mCaptionTextView.updateExistingData(localData.get(position), position);
            }*/

            if (!TextUtils.isEmpty(userName)) {
                ((ImageHolder) holder).mUsernameTextView.setText(userName);
            }

            bindImageView(((ImageHolder) holder), mTimeLineList.get(position), position);
        }

        if (position == mTimeLineList.size() - 1) {
            mLoadItems.loadMore(mTimeLineList.get(position).getId(), 11);
        }


    }

    private void bindImageView(ImageHolder imageHolder, BlinkTimeLineResponse blinkTimeLineResponse, int position) {
        String caption = blinkTimeLineResponse.getCaption();
        if (caption != null && caption.trim().length() > 0) {
            imageHolder.mCaptionTextView.setText(caption);
            imageHolder.mCaptionTextView.setVisibility(View.VISIBLE);
        } else {
            imageHolder.mCaptionTextView.setText("");
            //imageHolder.mCaptionTextView.setVisibility(View.GONE);
        }
        //imageHolder.mCaptionTextView.setCallback(callback, position);
        if (TextUtils.isEmpty(blinkTimeLineResponse.getVideo_url())) {

            RequestBuilder<Drawable> requestBuilder = Glide.with(imageHolder.itemView.getContext())
                    .load(blinkTimeLineResponse.getImage());

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.color.blackColor).diskCacheStrategy(DiskCacheStrategy.ALL);
            imageHolder.mProgressBar.setVisibility(View.VISIBLE);
            requestBuilder.
                    listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            imageHolder.mProgressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                            imageHolder.mProgressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).apply(requestOptions).
                    load(blinkTimeLineResponse.getImage()).into(imageHolder.mThumbnailImageView);

                /*
                Glide.with(itemView.getContext()).
                        applyDefaultRequestOptions(requestOptions).
                        load(blinkTimeLineResponse.getImage()).
                        into(mThumbnailImageView);*/
            imageHolder.mPlayIcon.setVisibility(View.GONE);
        } else {
            imageHolder.mPlayIcon.setVisibility(View.VISIBLE);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        imageHolder.mThumbnailImageView.setImageBitmap(retrieveVideoFrameFromVideo(blinkTimeLineResponse.getVideo_url()));
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            }).start();
        }
    }


    private Bitmap retrieveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap());
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());
        } finally {
            mediaMetadataRetriever.release();
        }
        return bitmap;
    }

    @Override
    public int getItemViewType(int position) {
        if (mTimeLineList.size() == 0) return -1;
        BlinkTimeLineResponse resp = mTimeLineList.get(position);

        if (!TextUtils.isEmpty(mTimeLineList.get(position).getVideo_url())) {
            return TYPE.VIDEO.ordinal();
        } else {
            return TYPE.IMAGE.ordinal();
        }
    }

    private class ImageHolder extends RecyclerView.ViewHolder {

        private ImageView mThumbnailImageView;
        private ImageView mPlayIcon;
        private ProgressBar mProgressBar;
        private ReadMoreTextView mCaptionTextView;
        private Button btnYes, btnNo;
        private TextView mUsernameTextView;

        private ImageHolder(View itemView) {
            super(itemView);
            mUsernameTextView = itemView.findViewById(R.id.tv_username);
            mThumbnailImageView = (ImageView) itemView.findViewById(R.id.iv_thumbnail);
            mPlayIcon = (ImageView) itemView.findViewById(R.id.iv_play);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
            mCaptionTextView = itemView.findViewById(R.id.tv_caption);
            btnYes = itemView.findViewById(R.id.btn_yes);
            btnNo = itemView.findViewById(R.id.btn_no);

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialog(v.getContext());
                }
            });

        }


    }

    private class VideoHolder extends RecyclerView.ViewHolder implements ToroPlayer {
        private final ImageView videoThumbImage, play_pause_img;
        private final ProgressBar mProgressBar;
        private PlayerView playerView;
        private LinearLayout mcCardView;
        LoopingPlayerHelper helper;
        Uri mediaUri;
        private Button btnYes, btnNo;
        private TextView mUsernameTextView;

        private ReadMoreTextView mCaptionTextView;
        private ImageView mPlatBtn;
        private boolean isPlaying;

        private VideoHolder(View itemView) {
            super(itemView);
            mUsernameTextView = itemView.findViewById(R.id.tv_username);
            playerView = itemView.findViewById(R.id.exoplayer);
            videoThumbImage = itemView.findViewById(R.id.video_thumb);
            play_pause_img = itemView.findViewById(R.id.play_pause_img);
            play_pause_img.setTag("play");
            mCaptionTextView = itemView.findViewById(R.id.tv_caption);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
            //playerView.setControllerAutoShow(false);
            btnYes = itemView.findViewById(R.id.btn_yes);
            btnNo = itemView.findViewById(R.id.btn_no);

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialog(v.getContext());
                }
            });

            mPlatBtn = (ImageView) itemView.findViewById(R.id.btn_play);
            mPlatBtn.setVisibility(View.GONE);
            playerView.setActivated(false);
            //playerView.setActivated(false);

            playerView.setSaveEnabled(true);
            mcCardView = itemView.findViewById(R.id.cv_video);

            mcCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (helper != null && helper.isPlaying() && play_pause_img.getVisibility() == View.GONE) {
                        play_pause_img.setVisibility(View.VISIBLE);
                        play_pause_img.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                        play_pause_img.setTag("pause");
                    }
                }
            });
            play_pause_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (helper != null) {
                        String tag = (String) play_pause_img.getTag();
                        if (tag != null && tag.equals("play")) {
                            helper.play();
                            //playerView.setVisibility(View.VISIBLE);
                            //videoThumbImage.setVisibility(View.INVISIBLE);
                            mProgressBar.setVisibility(View.VISIBLE);
                            play_pause_img.setVisibility(View.INVISIBLE);
                        } else if (helper.isPlaying()) {
                            helper.pause();
                            playerView.setVisibility(View.INVISIBLE);
                            videoThumbImage.setVisibility(View.VISIBLE);
                            play_pause_img.setVisibility(View.VISIBLE);
                            mProgressBar.setVisibility(View.GONE);
                            play_pause_img.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
                            play_pause_img.setTag("play");
                        }
                    }
                }
            });
            playerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (helper != null && helper.isPlaying() && play_pause_img.getVisibility() == View.GONE) {
                        play_pause_img.setVisibility(View.VISIBLE);
                        play_pause_img.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                        play_pause_img.setTag("pause");
                    }
                }
            });
        }

        @NonNull
        @Override
        public View getPlayerView() {
            return playerView;
        }

        @NonNull
        @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
            if (helper == null) {

                helper = new LoopingPlayerHelper(this, mediaUri);

                ((ExoPlayerViewHelper) helper).addEventListener(new Playable.EventListener() {
                    @Override
                    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

                    }

                    @Override
                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                        Log.d("onTracksChanged", "--------------" + trackSelections);
                    }

                    @Override
                    public void onLoadingChanged(boolean isLoading) {
                        Log.d("onLoadingChanged", "======>" + isLoading);
                        if (!isLoading) {
                            mProgressBar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        Log.d("onPlayerStateChanged", playWhenReady + "---------------" + playbackState);//2->buffering, 3->buffered, 4->completed
                        if (playWhenReady && playbackState == 3) {
                            mProgressBar.setVisibility(View.GONE);
                            playerView.setVisibility(View.VISIBLE);
                            videoThumbImage.setVisibility(View.INVISIBLE);
                            mPlatBtn.setImageResource(R.drawable.exo_controls_pause);
                            play_pause_img.setVisibility(View.INVISIBLE);
                        } else if (playWhenReady && playbackState == 4) {

                            /*if (helper != null) {
                                helper.getLatestPlaybackInfo().reset();
                                helper.initialize(container, playbackInfo);
                            }*/
                        }
                    }

                    @Override
                    public void onRepeatModeChanged(int repeatMode) {
                        Log.d("onRepeatModeChanged", "========" + repeatMode);
                    }

                    @Override
                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {
                        Log.e("onPlayerError", "========" + error.getMessage());
                    }

                    @Override
                    public void onPositionDiscontinuity(int reason) {
                        Log.e("onPositionDiscontinuity", "=====--===" + reason);
                    }

                    @Override
                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                    }

                    @Override
                    public void onSeekProcessed() {

                    }

                    @Override
                    public void onMetadata(Metadata metadata) {

                    }

                    @Override
                    public void onCues(List<Cue> cues) {

                    }

                    @Override
                    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
                        Log.d("onVideoSizeChanged", height + "--------------" + width);
                    }

                    @Override
                    public void onRenderedFirstFrame() {
                        playerView.setVisibility(View.VISIBLE);
                        videoThumbImage.setVisibility(View.INVISIBLE);
                        play_pause_img.setVisibility(View.INVISIBLE);
                    }
                });
            }
            helper.initialize(container, playbackInfo);

        }

        @Override
        public void play() {
            if (helper != null) {
                helper.play();
                mProgressBar.setVisibility(View.VISIBLE);
                play_pause_img.setVisibility(View.GONE);
                //playerView.setVisibility(View.VISIBLE);
                //videoThumbImage.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void pause() {
            if (helper != null) {
                helper.pause();
                playerView.setVisibility(View.INVISIBLE);
                videoThumbImage.setVisibility(View.VISIBLE);
                play_pause_img.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public boolean isPlaying() {
            return helper != null && helper.isPlaying();
        }

        @Override
        public void release() {
            if (helper != null) {
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }




        @Override
        public String toString() {
            return "ExoPlayer{" + hashCode() + " " + getAdapterPosition() + "}";
        }

        void bind(Uri media) {
            this.mediaUri = media;
        }


    }

    private void openDialog(Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_timeline);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        progressBarHorizontal = dialog.findViewById(R.id.timeline_progressBar2);

        ConstraintLayout parentLayout = dialog.findViewById(R.id.popup_parent_view);

        parentLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        isUpCalled = true;
                        validateTimer();

                        break;
                    case MotionEvent.ACTION_DOWN:
                        isUpCalled = false;
                        /*new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                if (!isUpCalled) {
                                    mCounter = 0;
                                }
                            }
                        }, 2000);*/
                        break;
                }
                return true;
            }
        });

        Button okBtn = dialog.findViewById(R.id.tv_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (timer != null) {
                    timer.cancel();
                }
            }
        });
        dialog.show();
        if (timer != null) {
            timer.cancel();
        }
        mCounter = 0;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isUpCalled)
                    mCounter++;
                validateTimer();
            }
        }, 100, 100);

    }

    private void validateTimer() {
        int progress = mCounter;
        if (progressBarHorizontal != null) {
            progressBarHorizontal.setProgress(progress);
        }
        if (mCounter >= 100 || (dialog != null && !dialog.isShowing())) {
            if (isUpCalled) {
                if (timer != null)
                    timer.cancel();
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        }
    }


}