package in.tvac.akshaye.lapichat.utils;

public class GlobalConstants {

    public static final String USER_MOBILE_ID = "user_mobile_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PASSWORD = "user_password_id";
    public static final String USER_ID = "user_id";
    public static final String USER_IMAGE = "user_image_url";

    public static final String USER_REMEMBER_ME = "remember_me";

    public static final String BASE_URL_STRING_REQ = "https://us-central1-ilookh.cloudfunctions.net/";
}
