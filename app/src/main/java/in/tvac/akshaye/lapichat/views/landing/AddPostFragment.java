package in.tvac.akshaye.lapichat.views.landing;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kbeanie.imagechooser.api.*;
import in.tvac.akshaye.lapichat.LapichatApp;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.pref.AppPreferencesHelper;
import in.tvac.akshaye.lapichat.databinding.FragmentAddPostBinding;
import in.tvac.akshaye.lapichat.utils.*;
import in.tvac.akshaye.lapichat.utils.FileUtils;
import in.tvac.akshaye.lapichat.views.BaseFragment;
import in.tvac.akshaye.lapichat.views.camera.CameraActivity;
import in.tvac.akshaye.lapichat.views.camera.VideoTrimmerActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static in.tvac.akshaye.lapichat.LapichatApp.userId;

public class AddPostFragment extends BaseFragment implements MediaChooserListener {

    private static final int ALL_PERMISSIONS_RESULT = 7777;
    private static final int REQUEST_PERMISSION_SETTING = 888;
    private static final int TRIM_VIDEO_CODE = 999;
    private static final int CAMERA_MEDIA = 555;
    private FragmentAddPostBinding binding;
    private int chooserType;
    private MediaChooserManager mediachooserManager;
    private String filePath = "";
    private ArrayList<String> permissionsToRequest = new ArrayList<>();
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private Bitmap mImageBitmap = null;
    private File mVideoFile = null;
    //Firebase
    FirebaseStorage storage;
    StorageReference storageReference;
    private ProgressDialog progressDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_post;
    }

    @Override
    protected void initViews(View view) {
        binding = (FragmentAddPostBinding) viewDataBinding;
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        binding.cameraIcon.setOnClickListener(v -> {
            permissions = new ArrayList<>();
            permissions.clear();
            permissions.add(READ_EXTERNAL_STORAGE);
            permissions.add(WRITE_EXTERNAL_STORAGE);
            if (checkAllPermisionsGranted()) {
                Intent intent = new Intent(mContext, CameraActivity.class);
                startActivityForResult(intent, CAMERA_MEDIA);
            }
        });
        SharedPreferences preferences = AppPreferencesHelper.getPref(mContext);
        String userImage = preferences.getString(GlobalConstants.USER_IMAGE, "");


        RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_person_black_24dp).
                diskCacheStrategy(DiskCacheStrategy.ALL).
                error(R.drawable.ic_person_black_24dp);
        if (!TextUtils.isEmpty(userImage))
            Glide.with(this).load(userImage).apply(requestOptions).
                    into(binding.userImg);


        binding.galleryIcon.setOnClickListener(v -> {
            permissions = new ArrayList<>();
            permissions.clear();

            permissions.add(READ_EXTERNAL_STORAGE);
            permissions.add(WRITE_EXTERNAL_STORAGE);
            if (checkAllPermisionsGranted()) {
                selectImage();
            }
        });

        binding.crossImg.setOnClickListener(v -> showIconLayout());
        binding.postBtn.setOnClickListener(v -> validateData(v));
    }

    private void validateData(View view) {
        if (mImageBitmap == null) {
            Toast.makeText(mContext, "Please select media to create new post.", Toast.LENGTH_LONG).show();
        } else if (NetworkUtils.isNetworkConnected(mContext)) {
            Utilities.hideKeypad(view);
            uploadPostOnFirebase();
        } else {
            Toast.makeText(mContext, "No Internet Connection.", Toast.LENGTH_LONG).show();
        }

    }

    private void uploadPostOnFirebase() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();
        if (mVideoFile == null) {
            uploadImage("");
        } else {
            uploadVideo();
        }
    }

    private void uploadVideo() {

        byte[] videoData = FileUtils.convertVideoPathToByteArray(mVideoFile);
        StorageReference ref = storageReference.child("videos/" + UUID.randomUUID().toString());
        ref.putBytes(videoData)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // progressDialog.dismiss();
                        Task<Uri> downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        downloadUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                //Toast.makeText(mContext, "Uploaded at: " + uri, Toast.LENGTH_SHORT).show();
                                uploadImage(uri.toString());
                            }
                        });

                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();

                        Toast.makeText(mContext, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void uploadImage(String videoUrl) {
        String childName = "";
        if (videoUrl == null || videoUrl.length() == 0) {
            childName = "images/" + UUID.randomUUID().toString();
        } else {
            childName = "videosThumb/" + UUID.randomUUID().toString();
        }

        StorageReference ref = storageReference.child(childName);
        byte[] imageData = FileUtils.convertThumbBitmapToByteArray(mImageBitmap);
        ref.putBytes(imageData)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.dismiss();
                        Task<Uri> downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        downloadUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                //Toast.makeText(mContext, "Uploaded at: " + uri, Toast.LENGTH_SHORT).show();
                                uploadText(videoUrl, uri.toString());
                            }
                        });
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                        progressDialog.setMessage("Uploaded " + (int) progress + "%");
                    }
                });

    }

    private void uploadText(String videoUrl, String imageUrl) {

        AppLogger.d("videoUrl->" + videoUrl);
        AppLogger.d("imageUrl=>" + imageUrl);

        if (NetworkUtils.isNetworkConnected(mContext)) {
            showDialog();

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            HashMap<String, Object> map = new HashMap<>();
            SharedPreferences preferences = AppPreferencesHelper.getPref(getBaseActivity());

            String userName = preferences.getString(GlobalConstants.USER_NAME, "");
            map.put("username", userName);
            map.put("image", imageUrl);


            if (!TextUtils.isEmpty(videoUrl))
                map.put("video_url", videoUrl);

            if (!TextUtils.isEmpty(binding.caption.getText().toString().trim())) {
                map.put("caption", binding.caption.getText().toString());
            }

            String user_id = preferences.getString(GlobalConstants.USER_ID, "");
            DatabaseReference myRef = database.getReference("Timeline").child(user_id).push();
            myRef.updateChildren(map, (databaseError, databaseReference) -> {
                hideDialog();
                if (databaseError == null) {
                    showIconLayout();
                    binding.caption.setText("");
                    Toast.makeText(activity, "Post Uploaded successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }

            });
        } else {
            Toast.makeText(mContext, "No Internet Connection.", Toast.LENGTH_LONG).show();
        }

    }


    private boolean checkAllPermisionsGranted() {
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();
        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (ActivityCompat.checkSelfPermission(mContext, permission) == PackageManager.PERMISSION_GRANTED);
            } else {
                AppLogger.e("PostImage", "PERMISSION GRANTED");
            }
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                if (permissionsToRequest != null) {
                    for (String perms : permissionsToRequest) {
                        if (hasPermission(perms)) {

                        } else {
                            permissionsRejected.add(perms);
                        }
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Need Storage Permission");
                            builder.setMessage("This app needs storage permission.");
                            builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    //sentToSettings = true;
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                                    //Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    }
                } else {
                    selectImage();
                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mContext)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void selectImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE_OR_VIDEO;
        if (mediachooserManager == null) {
            mediachooserManager = new MediaChooserManager(AddPostFragment.this, ChooserType.REQUEST_PICK_PICTURE_OR_VIDEO);
        }
        mediachooserManager.setMediaChooserListener(AddPostFragment.this);
        mediachooserManager.clearOldFiles();
        try {
            //pbar.setVisibility(View.VISIBLE);
            filePath = mediachooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //pbar.setVisibility(View.GONE);
        //super.onActivityResult(requestCode, resultCode, data);
        //AIzaSyCqB55zpm4yKMeu3IlzanhzIrUL9R7_aIM
        if (resultCode == Activity.RESULT_OK) {

            if (resultCode == RESULT_OK && requestCode == ChooserType.REQUEST_PICK_PICTURE_OR_VIDEO) {
                //binding.postLoader.setVisibility(View.VISIBLE);
                if (mediachooserManager == null) {
                    mediachooserManager = new MediaChooserManager(this, ChooserType.REQUEST_PICK_PICTURE_OR_VIDEO);
                }
                showImageLayout();
                mediachooserManager.submit(requestCode, data);
            } else if (data != null && (requestCode == TRIM_VIDEO_CODE || requestCode == CAMERA_MEDIA)) {

                //binding.postLoader.setVisibility(View.VISIBLE);
                Uri croppedVideoUri = data.getData();
                //int position = data.getIntExtra("position", 0);
                String videoPath = data.getStringExtra("videoFilePath");
                if (videoPath == null || videoPath.length() == 0) {
                    String imagePath = data.getStringExtra("imageFilePath");
                    if (imagePath != null && imagePath.length() > 0) {
                        showImageLayout();
                        Uri imageUri = Uri.fromFile(new File(imagePath));
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                            if (bitmap != null) {
                                mImageBitmap = bitmap;
                                binding.thumnailImg.setImageBitmap(bitmap);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            showIconLayout();
                        }
                    } else {
                        showIconLayout();
                    }
                } else {
                    AppLogger.d("result-", videoPath);
                    showImageLayout();
                    mVideoFile = new File(videoPath);
                    Bitmap videoThumbnail = getBitmapFromVideo(videoPath, croppedVideoUri);
                    if (videoThumbnail != null) {
                        binding.thumnailImg.setImageBitmap(videoThumbnail);
                        mImageBitmap = videoThumbnail;
                    } else {
                        showIconLayout();
                    }
                }
            }
        }
    }

    private Bitmap getBitmapFromVideo(String videoPath, Uri uri) {
        Bitmap thumbnailImg = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);

        if (thumbnailImg == null) {
            //Toast.makeText(mContext, "Unable to read video : " + realPath, Toast.LENGTH_LONG).show();
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(mContext, uri);
            thumbnailImg = mediaMetadataRetriever.getFrameAtTime(MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        }

        return thumbnailImg;
    }

    @Override
    public void onImageChosen(ChosenImage image) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                //binding.postLoader.setVisibility(View.GONE);
                if (image != null) {
                    LapichatApp.mediaType = "gallery";
                    Log.i("onImageChosen", "Chosen Image: Is not null");
                    //Toast.makeText(mContext, image.getFilePathOriginal(), Toast.LENGTH_LONG).show();
                    showImageLayout();
                    Uri imageUri = Uri.fromFile(new File(image.getFilePathOriginal()));
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        if (bitmap != null) {
                            mImageBitmap = bitmap;
                            binding.thumnailImg.setImageBitmap(bitmap);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        showIconLayout();
                    }
                } else {
                    Log.i("onImageChosen", "Chosen Image: Is null");
                    showIconLayout();
                }
            }
        });
    }

    private void showImageLayout() {
        binding.iconsLayout.setVisibility(View.GONE);
        binding.showImageRelate.setVisibility(View.VISIBLE);
    }

    private void showIconLayout() {
        binding.iconsLayout.setVisibility(View.VISIBLE);
        binding.showImageRelate.setVisibility(View.GONE);
        mImageBitmap = null;
        mVideoFile = null;
    }

    @Override
    public void onVideoChosen(ChosenVideo video) {
        String videoPath = video.getVideoFilePath();
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                //Toast.makeText(mContext, videoPath, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(mContext, VideoTrimmerActivity.class);
                intent.putExtra("REAL_VIDEO_PATH", videoPath);
                Bundle bundle = null;
                if (bundle == null) {
                    bundle = new Bundle();
                }
                bundle.putString("REAL_VIDEO_PATH", videoPath);
                intent.putExtras(bundle);
                startActivityForResult(intent, TRIM_VIDEO_CODE);
            }
        });
        /**/
    }

    @Override
    public void onError(String reason) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, reason, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onVideosChosen(ChosenVideos videos) {

    }

    @Override
    public void onImagesChosen(ChosenImages images) {

    }
}
