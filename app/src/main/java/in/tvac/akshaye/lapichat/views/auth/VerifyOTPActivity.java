package in.tvac.akshaye.lapichat.views.auth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.*;
import com.google.firebase.database.*;
import in.tvac.akshaye.lapichat.LapichatApp;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.pref.AppPreferencesHelper;
import in.tvac.akshaye.lapichat.utils.GlobalConstants;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class VerifyOTPActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText editTextCode;
    private String mVerificationId;
    private String mNumber;
    //private EditText numberEt;
    //private CountryCodePicker countryPicker;
    private TextView textView;
    private ProgressBar progressBar;
    private String password, name, dob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        //initializing objects
        mAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressbar);
        editTextCode = findViewById(R.id.editTextCode);
        //numberEt = findViewById(R.id.edit_text_number);
        textView = findViewById(R.id.textView);
        //countryPicker = findViewById(R.id.ccp);
        //countryPicker.setCountryForPhoneCode(91);
        //getting mobile number from the previous activity
        //and sending the verification code to the number
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        dob = intent.getStringExtra("dob");
        String mobile = intent.getStringExtra("mobile");
        password = intent.getStringExtra("password");

        mNumber = mobile;
        sendVerificationCode(mobile);

        //if the automatic sms detection did not work, user can also enter the code manually
        //so adding a click listener to the button
        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();

                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError("Enter valid code");
                    editTextCode.requestFocus();
                    return;
                }
                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });
    }

    //the method is sending verification code
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String mobile) {
        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);*/
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks
        );
    }

    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            } else {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(VerifyOTPActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            //super.onCodeSent(s, forceResendingToken);
            textView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        progressBar.setVisibility(View.VISIBLE);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(VerifyOTPActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            final FirebaseDatabase database = FirebaseDatabase.getInstance();

                            DatabaseReference ref = database.getReference();

                            //ref.child("users").orderByChild("mobile").endAt("289269");

                            Query query = ref.child("users").orderByChild("mobile").equalTo(mNumber);
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        // 1 or more users exist which have the username property "usernameToCheckIfExists"

                                        for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                            Log.d("-2-", "" + dataSnapshot2.getRef().getKey());
                                            goToNext(dataSnapshot2.getRef().getKey());
                                        }
                                        //dataSnapshot.getRef().getKey();
                                    } else {
                                        newSignUp(database);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.d("--", "" + databaseError.toString());
                                    progressBar.setVisibility(View.GONE);
                                }
                            });

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            } else {
                                message = task.getException().getMessage();
                            }

                            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
                        }
                    }
                });
    }

    private void newSignUp(FirebaseDatabase database) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", name);
        map.put("mobile", mNumber);
        map.put("dob", dob);
        map.put("password", password);

        DatabaseReference myRef = database.getReference("users").push();
        myRef.updateChildren(map, (databaseError, databaseReference) -> {
            //hideDialog();
            if (databaseError == null) {
                //showIconLayout();
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("--", "" + dataSnapshot.getRef().getKey());
                        goToNext(dataSnapshot.getRef().getKey());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressBar.setVisibility(View.GONE);
                        Log.d("--", "" + databaseError.toString());
                        Toast.makeText(VerifyOTPActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                //goToNext();
                //binding.caption.setText("");

            } else {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(VerifyOTPActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void goToNext(String refId) {
        SharedPreferences preferences = AppPreferencesHelper.getPref(VerifyOTPActivity.this);
        LapichatApp.userId = refId;
        preferences.edit().putString(GlobalConstants.USER_MOBILE_ID, mNumber).commit();
        preferences.edit().putString(GlobalConstants.USER_ID, refId).commit();
        preferences.edit().putString(GlobalConstants.USER_NAME,name).commit();

        //verification successful we will start the profile activity
        Intent intent = new Intent(VerifyOTPActivity.this, InviteFriendActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        Toast.makeText(VerifyOTPActivity.this, "Login successfully", Toast.LENGTH_SHORT).show();
    }
}
