package in.tvac.akshaye.lapichat.views.landing;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kbeanie.imagechooser.api.*;
import in.tvac.akshaye.lapichat.LapichatApp;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.pref.AppPreferencesHelper;
import in.tvac.akshaye.lapichat.databinding.FragmentProfileBinding;
import in.tvac.akshaye.lapichat.utils.AppLogger;
import in.tvac.akshaye.lapichat.utils.FileUtils;
import in.tvac.akshaye.lapichat.utils.GlobalConstants;
import in.tvac.akshaye.lapichat.views.BaseFragment;
import in.tvac.akshaye.lapichat.views.camera.CameraActivity;
import in.tvac.akshaye.lapichat.views.profile.MyFriendActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment implements ImageChooserListener {

    private static final int ALL_PERMISSIONS_RESULT = 7777;
    private static final int REQUEST_PERMISSION_SETTING = 888;
    private static final int TRIM_VIDEO_CODE = 999;
    private static final int CAMERA_MEDIA = 555;
    private FragmentProfileBinding binding;
    //private int chooserType;
    private ImageChooserManager mediachooserManager;
    private String filePath = "";
    private ArrayList<String> permissionsToRequest = new ArrayList<>();
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private Bitmap mImageBitmap = null;
    //private File mVideoFile = null;
    //Firebase
    FirebaseStorage storage;
    StorageReference storageReference;
    private ProgressDialog progressDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void initViews(View view) {
        binding = (FragmentProfileBinding) viewDataBinding;
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();


        binding.plusImage.setOnClickListener(v -> {
            permissions = new ArrayList<>();
            permissions.clear();
            permissions.add(READ_EXTERNAL_STORAGE);
            permissions.add(WRITE_EXTERNAL_STORAGE);
            if (checkAllPermisionsGranted()) {
                Intent intent = new Intent(mContext, CameraActivity.class);
                startActivityForResult(intent, CAMERA_MEDIA);
            }
        });

        binding.userImageProfile.setOnClickListener(v -> {
            permissions = new ArrayList<>();
            permissions.clear();

            permissions.add(READ_EXTERNAL_STORAGE);
            permissions.add(WRITE_EXTERNAL_STORAGE);
            if (checkAllPermisionsGranted()) {
                selectImage();
            }
        });

        binding.friendsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, MyFriendActivity.class));
            }
        });
        SharedPreferences preferences = AppPreferencesHelper.getPref(mContext);
        String username = preferences.getString(GlobalConstants.USER_NAME, "");
        String userImage = preferences.getString(GlobalConstants.USER_IMAGE, "");

        RequestOptions requestOptions = new RequestOptions().
                placeholder(R.drawable.ic_account_circle_black_24dp).
                diskCacheStrategy(DiskCacheStrategy.ALL).
                error(R.drawable.ic_account_circle_black_24dp);

        Glide.with(this).load(userImage).apply(requestOptions).into(binding.userImageProfile);
//        if (!TextUtils.isEmpty(userImage))

        binding.usernameTv.setText(username);

    }


    private void uploadImageOnFirebase() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();
        //if (mVideoFile == null) {
        uploadImage();

    }


    private void uploadImage() {
        String childName = "";
        //if (videoUrl == null || videoUrl.length() == 0) {
        childName = "userImages/" + UUID.randomUUID().toString();


        StorageReference ref = storageReference.child(childName);
        byte[] imageData = FileUtils.convertThumbBitmapToByteArray(mImageBitmap);
        ref.putBytes(imageData)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.dismiss();
                        Task<Uri> downloadUri = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        downloadUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                //Toast.makeText(mContext, "Uploaded at: " + uri, Toast.LENGTH_SHORT).show();
                                uploadText(uri.toString());

                            }
                        });
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                        progressDialog.setMessage("Uploaded " + (int) progress + "%");
                    }
                });

    }

    private void uploadText(String imageUrl) {
        //===============
        //String taskId = "-K1NRz9l5PU_0CFDtgXz";
        SharedPreferences preferences = AppPreferencesHelper.getPref(mContext);
        String user_id = preferences.getString(GlobalConstants.USER_ID, "");

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        HashMap<String,Object> map=new HashMap<>();
        map.put("user_image",imageUrl);

        DatabaseReference usersRef = database.getReference("users").child(user_id);
        usersRef.updateChildren(map, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError==null){
                    SharedPreferences preferences = AppPreferencesHelper.getPref(getBaseActivity());
                    preferences.edit().putString(GlobalConstants.USER_IMAGE, imageUrl).commit();
                    Toast.makeText(activity, "Profile picture uploaded successfully", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private boolean checkAllPermisionsGranted() {
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();
        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (ActivityCompat.checkSelfPermission(mContext, permission) == PackageManager.PERMISSION_GRANTED);
            } else {
                AppLogger.e("PostImage", "PERMISSION GRANTED");
            }
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                if (permissionsToRequest != null) {
                    for (String perms : permissionsToRequest) {
                        if (hasPermission(perms)) {

                        } else {
                            permissionsRejected.add(perms);
                        }
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Need Storage Permission");
                            builder.setMessage("This app needs storage permission.");
                            builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    //sentToSettings = true;
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                                    //Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    }
                } else {
                    selectImage();
                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mContext)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void selectImage() {
        //chooserType = ChooserType.REQUEST_PICK_PICTURE;
        if (mediachooserManager == null) {
            mediachooserManager = new ImageChooserManager(ProfileFragment.this, ChooserType.REQUEST_PICK_PICTURE);
        }
        mediachooserManager.setImageChooserListener(ProfileFragment.this);
        mediachooserManager.clearOldFiles();
        try {
            //pbar.setVisibility(View.VISIBLE);
            filePath = mediachooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //pbar.setVisibility(View.GONE);
        //super.onActivityResult(requestCode, resultCode, data);
        //AIzaSyCqB55zpm4yKMeu3IlzanhzIrUL9R7_aIM
        if (resultCode == Activity.RESULT_OK) {

            if (resultCode == RESULT_OK && requestCode == ChooserType.REQUEST_PICK_PICTURE) {
                //binding.postLoader.setVisibility(View.VISIBLE);
                if (mediachooserManager == null) {
                    mediachooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
                }
                //showImageLayout();
                mediachooserManager.submit(requestCode, data);
            } else if (data != null && (requestCode == TRIM_VIDEO_CODE || requestCode == CAMERA_MEDIA)) {

                //binding.postLoader.setVisibility(View.VISIBLE);
                //Uri croppedVideoUri = data.getData();
                //int position = data.getIntExtra("position", 0);

                String imagePath = data.getStringExtra("imageFilePath");
                if (imagePath != null && imagePath.length() > 0) {
                    //showImageLayout();
                    Uri imageUri = Uri.fromFile(new File(imagePath));
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        if (bitmap != null) {
                            mImageBitmap = bitmap;
                            binding.userImageProfile.setImageBitmap(bitmap);
                            uploadImageOnFirebase();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        //showIconLayout();
                    }
                } else {
                    //showIconLayout();
                }

            }
        }
    }


    @Override
    public void onImageChosen(ChosenImage image) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                //binding.postLoader.setVisibility(View.GONE);
                if (image != null) {
                    LapichatApp.mediaType = "gallery";
                    Log.i("onImageChosen", "Chosen Image: Is not null");
                    //Toast.makeText(mContext, image.getFilePathOriginal(), Toast.LENGTH_LONG).show();
                    //showImageLayout();
                    Uri imageUri = Uri.fromFile(new File(image.getFilePathOriginal()));
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        if (bitmap != null) {
                            mImageBitmap = bitmap;
                            binding.userImageProfile.setImageBitmap(bitmap);
                            uploadImageOnFirebase();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        //showIconLayout();
                    }
                } else {
                    Log.i("onImageChosen", "Chosen Image: Is null");
                    //showIconLayout();
                }
            }
        });
    }


    @Override
    public void onError(String reason) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, reason, Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onImagesChosen(ChosenImages images) {

    }
}
