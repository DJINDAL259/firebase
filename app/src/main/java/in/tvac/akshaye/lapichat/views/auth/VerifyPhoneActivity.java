package in.tvac.akshaye.lapichat.views.auth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.*;
import com.hbb20.CountryCodePicker;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.pref.AppPreferencesHelper;
import in.tvac.akshaye.lapichat.views.landing.LandingActivity;

import java.util.concurrent.TimeUnit;

public class VerifyPhoneActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText editTextCode;
    private String mVerificationId;
    // private String mNumber;
    private EditText numberEt;
    private CountryCodePicker countryPicker;
    private TextView textView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(in.tvac.akshaye.lapichat.R.layout.activity_verify_phone);

        //initializing objects
        mAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressbar);
        editTextCode = findViewById(in.tvac.akshaye.lapichat.R.id.editTextCode);
        numberEt = findViewById(R.id.edit_text_number);
        textView = findViewById(R.id.textView);
        countryPicker = findViewById(R.id.ccp);
        countryPicker.setCountryForPhoneCode(91);
        //getting mobile number from the previous activity
        //and sending the verification code to the number
        Intent intent = getIntent();
        String mobile = intent.getStringExtra("mobile");
        //mNumber = mobile;
        //sendVerificationCode(mobile);


        findViewById(R.id.btn_otp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SELECT id, email, name, mobile FROM `users` WHERE id>500
                String mobileNumber = numberEt.getText().toString().trim();
                if (mobileNumber.isEmpty() || mobileNumber.length() < 10) {
                    numberEt.setError("Please enter valid number");
                    numberEt.requestFocus();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                String prefix = "+" + countryPicker.getSelectedCountryCode();
                mobileNumber = prefix + mobileNumber;
                sendVerificationCode(mobileNumber);
            }
        });
        //if the automatic sms detection did not work, user can also enter the code manually
        //so adding a click listener to the button
        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                String number = numberEt.getText().toString().trim();
                if (number.isEmpty() || number.length() < 10) {
                    numberEt.setError("Please enter valid number");
                    numberEt.requestFocus();
                    return;
                }
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError("Enter valid code");
                    editTextCode.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });
    }

    //the method is sending verification code
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String mobile) {
        /*PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);*/
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks
        );
    }

    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            } else {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(VerifyPhoneActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            //super.onCodeSent(s, forceResendingToken);
            textView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(VerifyPhoneActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            SharedPreferences preferences = AppPreferencesHelper.getPref(VerifyPhoneActivity.this);
                            //preferences.edit().putString(GlobalConstants.USER_MOBILE_ID, mNumber).commit();
                            //verification successful we will start the profile activity
                            Intent intent = new Intent(VerifyPhoneActivity.this, LandingActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            } else {
                                message = task.getException().getMessage();
                            }

                            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
                        }
                    }
                });
    }
}
