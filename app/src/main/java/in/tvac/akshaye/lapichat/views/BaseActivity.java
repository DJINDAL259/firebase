package in.tvac.akshaye.lapichat.views;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.Toast;

public abstract class BaseActivity extends AppCompatActivity {
    protected ViewDataBinding viewDataBinding;
    private Fragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        initView();

    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    /**
     * This method is used to replace fragment .
     *
     * @param newFragment :replace an existing fragment with new fragment.
     * @param args        :pass bundle data fron one fragment to another fragment
     */
    public void replaceFragment(FrameLayout frameLayout, Fragment newFragment, Bundle args) {
        setFragment(newFragment);
        FragmentManager manager = getSupportFragmentManager();
        //manager.popBackStack();
        if (args != null)
            newFragment.setArguments(args);
        FragmentTransaction transaction = manager.beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(frameLayout.getId(), newFragment);
        //   transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    protected void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
