package in.tvac.akshaye.lapichat.views.profile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import com.google.firebase.database.*;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.adapter.MyFriendsAdapter;
import in.tvac.akshaye.lapichat.data.local.contacts.Contact;
import in.tvac.akshaye.lapichat.data.local.contacts.ContactFetcher;
import in.tvac.akshaye.lapichat.data.model.output.OutputAddress;
import in.tvac.akshaye.lapichat.databinding.ActivityAddFriendBinding;
import in.tvac.akshaye.lapichat.databinding.ActivityInviteFriendBinding;
import in.tvac.akshaye.lapichat.filters.SortContacts;
import in.tvac.akshaye.lapichat.presenter.PhoneValidatorPresenter;
import in.tvac.akshaye.lapichat.views.BaseActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddFriendActivity extends BaseActivity {


    private ActivityAddFriendBinding binding;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_friend;
    }

    //@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initView() {
        binding = (ActivityAddFriendBinding) viewDataBinding;

    }

}
