package in.tvac.akshaye.lapichat.data.local.contacts;

public class ContactPhone {
	public String number;
	public String type;

	public ContactPhone(String number, String type) {
		this.number = number;
		this.type = type;
	}
}
