package in.tvac.akshaye.lapichat.presenter;

import android.content.Context;
import android.util.Log;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import in.tvac.akshaye.lapichat.LapichatApp;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.contacts.Contact;
import in.tvac.akshaye.lapichat.data.model.input.InputAddresses;
import in.tvac.akshaye.lapichat.data.model.output.OutputAddress;
import in.tvac.akshaye.lapichat.utils.GlobalConstants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static im.ene.toro.PlayerSelector.TAG;

public class PhoneValidatorPresenter {


    private final Context mContext;
    private final LoginInterface mLoginCallback;

    public PhoneValidatorPresenter(Context context, LoginInterface loginInterface) {
        mContext = context;
        mLoginCallback = loginInterface;
    }

    public interface LoginInterface {
        void onError(String errorMessage);

        void onSuccess(List<OutputAddress.User> phoneNumbers);
    }

    public void validateNumbers(ArrayList<Contact> dbPhoneNumbers) {

        InputAddresses inputAddresses = new InputAddresses();
        List<InputAddresses.User> users = new ArrayList<>();
        for (int i = 0; i < dbPhoneNumbers.size(); i++) {
            if (dbPhoneNumbers.get(i).numbers != null && dbPhoneNumbers.get(i).numbers.size() > 0) {
                InputAddresses.User user = new InputAddresses.User();
                String phoneNumber = dbPhoneNumbers.get(i).numbers.get(0).number;
                if (phoneNumber.startsWith("0")) {
                    phoneNumber = phoneNumber.substring(1);
                }
                if (phoneNumber.contains(" ")) {
                    phoneNumber = phoneNumber.replace(" ", "");
                }
                if (!phoneNumber.contains("+")) {
                    phoneNumber = "+91" + phoneNumber;
                }
                user.setMobile(phoneNumber);
                users.add(user);
                dbPhoneNumbers.get(i).numbers.get(0).number = phoneNumber;
            } else {
                dbPhoneNumbers.get(i).isExist = false;
                dbPhoneNumbers.set(i, dbPhoneNumbers.get(i));
            }
        }
        inputAddresses.setUsers(users);
        Gson gson = new Gson();
        String jsonArrayString = gson.toJson(inputAddresses);
        Log.d("INPUT", "PARAMS:-" + jsonArrayString);
        /*
        GitHubService apiInterface = APIClient.getClient().create(GitHubService.class);
        apiInterface.getAddressStatus(inputAddresses).enqueue(new Callback<List<OutputAddress>>() {
            @Override
            public void onResponse(Call<List<OutputAddress>> call, Response<List<OutputAddress>> response) {
                mLoginCallback.onSuccess("Success");
            }

            @Override
            public void onFailure(Call<List<OutputAddress>> call, Throwable t) {
                mLoginCallback.onError(t.getMessage());
            }
        });*/

        Log.e("ar post  Api", GlobalConstants.BASE_URL_STRING_REQ + mContext.getResources().getString(R.string.mobile_list));
        StringRequest strReq = new StringRequest(Request.Method.POST,
                GlobalConstants.BASE_URL_STRING_REQ
                        + mContext.getResources()
                        .getString(R.string.mobile_list),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ar post response", response.toString());
                        OutputAddress output = gson.fromJson(response, OutputAddress.class);

                        List<OutputAddress.User> list = null;
                        if (output != null) {
                            list = output.getUsers();
                        }
                        mLoginCallback.onSuccess(list);
                        //resultCallback.onPageFollowSuccess("");
                        //processResult(response);
                        //resultCallback.onSuccess(count_res);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                String res = error.toString();
                Log.e("ar post Error:-", res);
                if (error.networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;

                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Content-Type", "application/json; charset=utf-8");
                return header;
            }


            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonArrayString.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return null;
            }
            // Adding request to request queue
        };
        LapichatApp.getInstance().addToRequestQueue(strReq, "mobile_status");

    }
}
