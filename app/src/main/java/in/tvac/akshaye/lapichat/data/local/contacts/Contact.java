package in.tvac.akshaye.lapichat.data.local.contacts;

import java.util.ArrayList;

public class Contact {
    public String id;
    public String name;
    public boolean isExist;
    public ArrayList<ContactEmail> emails;
    public ArrayList<ContactPhone> numbers;

    public String getName(){
        return name;
    }
    private void setName(String name){
        this.name=name;
    }

    public Contact(String id, String name) {
        this.id = id;
        this.name = name;
        this.isExist = false;
        this.emails = new ArrayList<ContactEmail>();
        this.numbers = new ArrayList<ContactPhone>();
    }

    @Override
    public String toString() {
        String result = name;
        if (numbers.size() > 0) {
            ContactPhone number = numbers.get(0);
            result += " (" + number.number + " - " + number.type + ")";
        }
        if (emails.size() > 0) {
            ContactEmail email = emails.get(0);
            result += " [" + email.address + " - " + email.type + "]";
        }
        return result;
    }

    public void addEmail(String address, String type) {
        emails.add(new ContactEmail(address, type));
    }

    public void addNumber(String number, String type) {
        numbers.add(new ContactPhone(number, type));
    }
}
