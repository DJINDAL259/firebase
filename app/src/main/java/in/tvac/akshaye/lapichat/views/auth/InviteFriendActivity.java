package in.tvac.akshaye.lapichat.views.auth;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import com.google.firebase.database.*;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.adapter.InviteFriendsAdapter;
import in.tvac.akshaye.lapichat.data.local.contacts.Contact;
import in.tvac.akshaye.lapichat.data.local.contacts.ContactFetcher;
import in.tvac.akshaye.lapichat.data.model.output.OutputAddress;
import in.tvac.akshaye.lapichat.databinding.ActivityInviteFriendBinding;
import in.tvac.akshaye.lapichat.filters.SortContacts;
import in.tvac.akshaye.lapichat.presenter.PhoneValidatorPresenter;
import in.tvac.akshaye.lapichat.utils.ThreadManager;
import in.tvac.akshaye.lapichat.views.BaseActivity;
import in.tvac.akshaye.lapichat.views.landing.LandingActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InviteFriendActivity extends BaseActivity implements PhoneValidatorPresenter.LoginInterface {

    private static final int REQUEST_READ_CONTACTS = 9876;
    private static final String TAG = InviteFriendActivity.class.getName();
    private ArrayList<Contact> listContacts;
    private RecyclerView lvContacts;
    private SwipeRefreshLayout swipeRefreshLayout;
    private InviteFriendsAdapter adapterContacts;
    private ActivityInviteFriendBinding binding;
    private TextView sync_tv;
    private int processingContactLimit = 50;
    private int currentIndex = 0;
    private boolean searching = false;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_invite_friend;
    }

    //@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initView() {
        binding = (ActivityInviteFriendBinding) viewDataBinding;
        binding.searchView.setOnCloseListener(() -> {
            adapterContacts.updateList(listContacts);
            binding.recyclerView.setVisibility(View.VISIBLE);
            binding.tvNoRecycler.setVisibility(View.GONE);
            return false;
        });
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference ref = database.getReference();


        binding.searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchView.requestFocus();
                binding.searchView.onActionViewExpanded();
                InputMethodManager im = ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE));
                im.showSoftInput(v, 0);
            }
        });

        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!TextUtils.isEmpty(s)) {
                    searching = true;
                    if (listContacts.size() > 0) {
                        ArrayList<Contact> filterList = new ArrayList<>();

                        for (Contact cont : listContacts) {
                            String name = cont.name.toLowerCase();
                            if (name.startsWith(s.toLowerCase())) {
                                filterList.add(cont);
                            }
                        }
                        if (filterList.size() > 0) {
                            adapterContacts.updateList(filterList);
                            binding.tvNoRecycler.setVisibility(View.GONE);
                            binding.recyclerView.setVisibility(View.VISIBLE);
                        } else {
                            binding.recyclerView.setVisibility(View.GONE);
                            binding.tvNoRecycler.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    searching = false;
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    binding.tvNoRecycler.setVisibility(View.GONE);
                    adapterContacts.updateList(listContacts);
                }
                return false;
            }
        });

        binding.skipTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InviteFriendActivity.this, LandingActivity.class));
                finish();
            }
        });

        lvContacts = findViewById(R.id.recyclerView);
        setAdapter();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        sync_tv = findViewById(R.id.sync_tv);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContacts();
            }
        });

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            getContacts();
        } else {
            requestLocationPermission();
        }
    }


    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(InviteFriendActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        lvContacts.setLayoutManager(linearLayoutManager);
    }

    protected void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
            // show UI part if you want here to show some rationale !!!
            String message = "Permission is required to see contact list";
            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Setting", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            });
            snackbar.show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getContacts();

                } else {
                    String message = "Permission is required to see contact list";
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("Agree", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestLocationPermission();
                        }
                    });
                    snackbar.show();
                    // permission denied,Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void getContacts() {

        binding.syncTv.setVisibility(View.GONE);
        listContacts = new ContactFetcher(InviteFriendActivity.this).fetchNumberOnly();
        swipeRefreshLayout.setRefreshing(false);

        Collections.sort(listContacts, new SortContacts());

        adapterContacts = new InviteFriendsAdapter(listContacts, InviteFriendActivity.this);
        lvContacts.setAdapter(adapterContacts);
        binding.tvNoRecycler.setVisibility(View.GONE);
        /**/

        currentIndex = 0;

        syncContact();
        /* for (int i = 0; i < listContacts.size(); i++) {
            updateInviteStatus(listContacts.get(i));
        }*/

       /* ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
            @Override
            public void onBackground() {
                Log.d("", "=>");
            }

            @Override
            public void onUi() {
                super.onUi();

            }
        });*/
    }

    private void syncContact() {
        ArrayList<Contact> processingList = new ArrayList<>();

        int length = currentIndex + processingContactLimit;

        for (int i = currentIndex; i < length; i++) {
            currentIndex++;
            if (i < listContacts.size()) {
                processingList.add(listContacts.get(i));
            } else {
                break;
            }
        }

        if (processingList.size() > 0) {
            new PhoneValidatorPresenter(InviteFriendActivity.this, InviteFriendActivity.this).validateNumbers(processingList);
        }
    }

    private void updateInviteStatus(final Contact contact) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference ref = database.getReference();

        String mNumber = contact.numbers.get(0).number;
        Query query = ref.child("users").orderByChild("mobile").equalTo(mNumber);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    Log.e(TAG, "key--->>" + dataSnapshot.getKey());
                    contact.isExist = true;
                } else {
                    // newSignUp(database);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("--", "" + databaseError.toString());
                //progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onError(String errorMessage) {
        if (adapterContacts != null) {
            if (!searching)
                adapterContacts.updateList(listContacts);
        } else {
            adapterContacts = new InviteFriendsAdapter(listContacts, InviteFriendActivity.this);
            lvContacts.setAdapter(adapterContacts);
        }
        syncContact();
        binding.tvNoRecycler.setVisibility(View.GONE);
        binding.syncTv.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(List<OutputAddress.User> list) {
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                OutputAddress.User data = list.get(i);
                if (data != null) {
                    String mobiNumber = data.getMobile();
                    for (int j = 0; j < listContacts.size(); j++) {
                        if (listContacts.get(j).numbers != null && listContacts.get(j).numbers.size() > 0) {
                            String phoneNumber = listContacts.get(j).numbers.get(0).number;
                            if (phoneNumber.equals(mobiNumber)) {
                                Boolean isExist = list.get(i).getIsSingedUp();
                                listContacts.get(j).isExist = isExist;
                                break;
                            }
                        }
                    }
                }
            }
        }


        if (adapterContacts != null) {
            if (!searching)
                adapterContacts.updateList(listContacts);
        } else {
            adapterContacts = new InviteFriendsAdapter(listContacts, InviteFriendActivity.this);
            lvContacts.setAdapter(adapterContacts);
        }
        syncContact();
        binding.tvNoRecycler.setVisibility(View.GONE);
        binding.syncTv.setVisibility(View.GONE);
    }
}
