package in.tvac.akshaye.lapichat.views.camera;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.MediaChooserManager;
import com.otaliastudios.cameraview.*;
import in.tvac.akshaye.lapichat.LapichatApp;
import in.tvac.akshaye.lapichat.MainActivity;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.utils.AppLogger;
import in.tvac.akshaye.lapichat.utils.FileUtils;
import in.tvac.akshaye.lapichat.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.RECORD_AUDIO;

public class CameraActivity extends AppCompatActivity {


    private static final int TRIM_VIDEO_CODE = 999;
    private CameraView mCameraView;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private boolean mCapturingVideo;
    private boolean cameraStarted;
    private boolean mCapturingPicture;
    private long mCaptureTime;
    private boolean flashOn;
    private Size mCaptureNativeSize;
    private ProgressDialog pDialog;
    private Bundle bundle;
    private boolean isUpCalled;
    private boolean isVideoRecording;
    private Timer timer;
    private boolean isCaptureRunning;
    private int count_time;
    private boolean camerRationalDailog;
    private ImageView backBtn;
    private ImageView changeCameraBtn;
    private ImageView flashCamera;
    private ImageView cameraCapture;
    private TextView timerText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        // binding = DataBindingUtil.setContentView(this, R.layout.activity_camera);
        setContentView(R.layout.activity_camera);

        CameraLogger.setLogLevel(CameraLogger.LEVEL_VERBOSE);
        backBtn = findViewById(R.id.back_btn);
        changeCameraBtn = findViewById(R.id.change_camera_btn);
        flashCamera = findViewById(R.id.flash_camera);
        cameraCapture = findViewById(R.id.camera_capture);
        timerText = findViewById(R.id.timer_text);
        backBtn = findViewById(R.id.back_btn);
        backBtn = findViewById(R.id.back_btn);
        backBtn = findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        changeCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCapturingPicture) return;
                switch (mCameraView.toggleFacing()) {
                    case BACK:
                        message("Switched to back camera!", false);
                        break;

                    case FRONT:
                        message("Switched to front camera!", false);
                        break;
                }
            }
        });
        flashCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flashOn) {
                    flashOn = false;
                    flashCamera.setImageResource(R.drawable.ic_flash_off_white);
                } else {
                    flashOn = true;
                    flashCamera.setImageResource(R.drawable.ic_flash_on_white);
                }
            }
        });


        mCameraView = (CameraView) findViewById(R.id.camera);
        mCameraView.setJpegQuality(100);

        mCameraView.addCameraListener(new CameraListener() {
            public void onCameraOpened(CameraOptions options) {
                //onOpened();

            }

            public void onPictureTaken(byte[] jpeg) {
                onPicture(jpeg);

            }

            @Override
            public void onVideoTaken(File video) {
                super.onVideoTaken(video);
                onVideo(video);
            }
        });

        registerTouchListener();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            try {
                mCameraView.start();
            } catch (Exception e) {

            }
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            //Toast.makeText(this, R.string.camera_permission_not_granted, Toast.LENGTH_SHORT).show();
            if (!camerRationalDailog)
                showCameraRationaleDialog();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
        //binding.postLoader.setVisibility(View.GONE);
    }

    private void showCameraRationaleDialog() {

        new android.app.AlertDialog.Builder(this)
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(CameraActivity.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(LocationBaseActivity.this, "Permission denied.", Toast.LENGTH_SHORT).show();
                        //mRequestingLocationUpdates = false;
                    }
                })
                .setCancelable(false)
                .setMessage("This application needs to allow use of camera permission!")
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        camerRationalDailog = false;
                    }
                })
                .show();
        camerRationalDailog = true;
    }

    private void registerTouchListener() {
        cameraCapture.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        isUpCalled = true;
                        if (isVideoRecording) {
                            stopTimer();
                        }
                        break;
                    case MotionEvent.ACTION_DOWN:
                        isUpCalled = false;

                        new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                CameraActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isUpCalled) {
                                            clickImage();
                                        } else {
                                            recordVideo();
                                        }
                                    }
                                });
                            }
                        }, 500);
                        break;
                }

                return true;
            }
        });
    }

    private ArrayList<String> permissionsToRequest = new ArrayList<>();
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSION = 107;

    private void recordVideo() {

        // permissions.add(READ_EXTERNAL_STORAGE);
        //permissions.add(WRITE_EXTERNAL_STORAGE);

        permissions = new ArrayList<>();
        permissions.clear();
        permissions.add(RECORD_AUDIO);

        if (checkAllPermisionsGranted()) {
            if (isVideoRecording) {
                stopTimer();
                return;
            }
            if (!isCaptureRunning) {
                isCaptureRunning = true;
                //video_recorder.setImageResource(R.drawable.ic_fiber_manual_stop_24dp);
                setCaptureTiming();
            }
        }

    }

    private boolean checkAllPermisionsGranted() {
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSION);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();
        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED);
            } else {
                AppLogger.e("Camera", "PERMISSION GRANTED");
            }
        }
        return true;
    }

    private void stopTimer() {
        try {
            timer.cancel();
            timer = null;
           /* if (count_time < 3 || count_time == 0) {
                timerTextView.setText("00:00");
                stopVideoRecording(false);
            }
            if (count_time >= 3) {
                stopVideoRecording(true);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            stopVideoRecording(true);
        } catch (Exception e) {

        }
    }


    private void setCaptureTiming() {
        try {
            if (mCameraView != null) {
                //recordVideo();
                mCameraView.setSessionType(SessionType.VIDEO);
                LapichatApp.recordedVideoPath = FileUtils.createVideoDirInSdCard("temp_" + System.currentTimeMillis(), this);
                captureVideo();
            }
            isCaptureRunning = false;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("", "" + e.getMessage());
            isCaptureRunning = false;
        }
    }

    private void captureVideo() {
        if (mCameraView.getSessionType() != SessionType.VIDEO) {
            message("Can't record video while session type is 'picture'.", false);
            return;
        }
        if (mCapturingPicture || mCapturingVideo) return;
        mCapturingVideo = true;
        message("Recording for 60 seconds...", true);
        mCameraView.setVideoMaxDuration(60000);
        //mCameraView.startCapturingVideo(null, 60000);
        mCameraView.startCapturingVideo(null);
        updateStartUI();
    }

    private void updateStartUI() {
        setTiming();
        isVideoRecording = true;
        //videoCapture.setTextColor(ContextCompat.getColor(PickMediaActivity.this, R.color.redColor));
        cameraCapture.setBackgroundResource(R.drawable.circle_redborder);

        cameraStarted = false;
    }

    private void setTiming() {
        try {
            if (timer == null) {
                timerText.setVisibility(View.VISIBLE);
                timerText.setText("00:00");
                count_time = 0;
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        count_time++;
                        CameraActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (count_time == 61) {
                                    timerText.setText("01:" + "00");
                                    timer.cancel();
                                    timer = null;
                                    stopVideoRecording(true);
                                } else if (count_time > 9) {
                                    timerText.setText("00:" + count_time);
                                } else {
                                    timerText.setText("00:0" + count_time);
                                }
                            }
                        });
                    }
                }, 500, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopVideoRecording(boolean flag) {
        try {
            count_time = 0;
            isVideoRecording = false;
            //video_recorder.setImageResource(R.drawable.ic_fiber_manual_record_24dp);
            if (flag == false) {
                Utilities.showlongToast(CameraActivity.this, "Video length should be between 3-60 seconds");
                timerText.setVisibility(View.GONE);
                cameraStarted = true;
                mCameraView.stopCapturingVideo();
            } else {
                if (mCameraView != null) {//&& isVideoRecording == true)
                    mCameraView.stopCapturingVideo();
                    updateStopUI();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (LapichatApp.recordedVideoPath != "" && LapichatApp.recordedVideoPath != null) {
                File file = new File(LapichatApp.recordedVideoPath);
                boolean deleted = file.delete();
                LapichatApp.recordedVideoPath = "";
            }
        }
    }

    private void updateStopUI() {
        cameraStarted = false;
        timerText.setText("00:00");
        timerText.setVisibility(View.GONE);
        //closeBtn.setVisibility(View.VISIBLE);
        cameraCapture.setBackgroundResource(R.drawable.roundicon);
    }


    private void clickImage() {

        try {
            //camera.takePicture(null, this, this);
            if (mCapturingPicture) return;
            mCapturingPicture = true;
            mCaptureTime = System.currentTimeMillis();
            if (flashOn) {
                mCameraView.setSessionType(SessionType.PICTURE);
                mCameraView.setFlash(Flash.TORCH);
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        CameraActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mCaptureNativeSize = mCameraView.getPictureSize();
                                mCameraView.capturePicture();
                            }
                        });
                    }
                }, 1000);
            } else {
                mCaptureNativeSize = mCameraView.getPictureSize();
                mCameraView.capturePicture();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onPicture(byte[] data) {
        mCapturingPicture = false;
        long callbackTime = System.currentTimeMillis();
        if (mCapturingVideo) {
            message("Captured while taking video. Size=" + mCaptureNativeSize, false);
            return;
        }

        // This can happen if picture was taken with a gesture.
        if (mCaptureTime == 0) mCaptureTime = callbackTime - 300;
        if (mCaptureNativeSize == null) mCaptureNativeSize = mCameraView.getPictureSize();

        if (data == null) {
            if (flashOn)
                mCameraView.setFlash(Flash.OFF);

            return;
        }
        //camera.getParameters().getHorizontalViewAngle();
        //isImageCaptured = true;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        //Logger.d("onPictureTaken", displayMetrics.heightPixels + "---" + screenWidth + "--" + SCREEN_ORIENTATION);
        Random rndm = new Random();
        int rndomNumber = rndm.nextInt();
        File imageFile = new File(getExternalFilesDir(null), rndomNumber + ".jpeg");
        //picturePath = imageFile.getAbsolutePath();
        saveImageLocally(data, imageFile);

    }

    private void message(String content, boolean important) {
        int length = important ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        Toast.makeText(this, content, length).show();
    }

    private void saveImageLocally(final byte[] source, final File imageFile) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(CameraActivity.this);
            pDialog.setMessage(getResources()
                    .getString(R.string.saving_image));
            pDialog.setCancelable(false);
        }
        showProgressDialog();
        CameraUtils.decodeBitmap(source, 3000, 3000, new CameraUtils.BitmapCallback() {
            @Override
            public void onBitmapReady(final Bitmap bmp) {
                //photo_saving_progress.setVisibility(View.VISIBLE);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Runtime.getRuntime().gc();
                            System.gc();
                            //
                            int QUALITY = 100;

                            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, QUALITY, stream1);
                            byte[] rotateddata = stream1.toByteArray();
                            FileUtils.saveToSdcard(rotateddata, imageFile);
                        } catch (Exception e) {
                            e.printStackTrace();
                            FileUtils.saveToSdcard(source, imageFile);
                        }
                        //final Bitmap finalBmp = bmp;
                        CameraActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                if (flashOn)
                                    mCameraView.setFlash(Flash.OFF);

                                Intent intent = new Intent();
                                intent.putExtra("imageFilePath", imageFile.getAbsolutePath());
                                setResult(RESULT_OK, intent);
                                finish();

                                LapichatApp.mediaType = "camera";
                            }
                        });
                    }
                });
                thread.start();
            }
        });
    }

    private void onVideo(File video) {
        mCapturingVideo = false;
        if (cameraStarted) {
            //File file = new File(EnlteApp.recordedVideoPath);
            boolean deleted = video.delete();
            LapichatApp.recordedVideoPath = "";
        } else {
            LapichatApp.mediaType = "camera";
            LapichatApp.recordedVideoPath = video.getAbsolutePath();
            Intent intent = new Intent(this, VideoTrimmerActivity.class);
            intent.putExtra("REAL_VIDEO_PATH", LapichatApp.recordedVideoPath);
            //intent.putExtra("hintname", hintname);
            startActivityForResult(intent, TRIM_VIDEO_CODE);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //pbar.setVisibility(View.GONE);
        super.onActivityResult(requestCode, resultCode, data);
        //AIzaSyCqB55zpm4yKMeu3IlzanhzIrUL9R7_aIM
        if (resultCode == RESULT_OK) {

            if (data != null && (requestCode == TRIM_VIDEO_CODE || requestCode == TRIM_VIDEO_CODE)) {
                //binding.postLoader.setVisibility(View.VISIBLE);
                Uri croppedVideoUri = data.getData();
                Intent intent = new Intent(Intent.ACTION_VIEW, croppedVideoUri);
                intent.setDataAndType(croppedVideoUri, "video/mp4");
                intent.putExtra("position", data.getIntExtra("position", 0));
                intent.putExtra("videoFilePath", data.getStringExtra("videoFilePath"));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    private void showProgressDialog() {
        if (pDialog != null) {
            pDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (pDialog != null) {
            try {
                pDialog.dismiss();
            } catch (Exception e) {
            }
            pDialog = null;

        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        mCameraView.stop();
        //binding.postLoader.setVisibility(View.GONE);
        hideProgressDialog();

    }

    protected void onDestroy() {
        super.onDestroy();
        if (mCameraView != null)
            mCameraView.destroy();
    }


    private static final int REQUEST_PERMISSION_SETTING = 50;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case ALL_PERMISSION:
                if (permissionsToRequest != null) {
                    for (String perms : permissionsToRequest) {
                        if (hasPermission(perms)) {

                        } else {
                            permissionsRejected.add(perms);
                        }
                    }
                }
                if (permissionsRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSION);
                                            }
                                        }
                                    });
                            return;
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle("Need Storage Permission");
                            builder.setMessage("This app needs storage permission.");
                            builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    //sentToSettings = true;
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                                    //Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    }
                } else {
                    if (permissions != null && permissions.length > 0 && permissions[0] == ACCESS_FINE_LOCATION) {
                        //--startService(new Intent(this, LocationService.class));
                    } else {//record audio

                    }
                    // saveImage();
                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
