package in.tvac.akshaye.lapichat.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class Utils {

    public static void inviteFriends(Activity context, String sharing_message) {
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        //String message = "Hey There I'm using Enlte! " + "\n" + "https://play.google.com/store/apps/details?id=com.mpas.enlte&hl=en" + "\n" + "#Enlte  #Power To You";
        sendIntent.putExtra(Intent.EXTRA_TEXT, sharing_message);
        context.startActivity(sendIntent);
    }
}
