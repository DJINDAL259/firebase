package in.tvac.akshaye.lapichat.views.camera;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import in.tvac.akshaye.lapichat.R;
import life.knowledge4.videotrimmer.K4LVideoTrimmer;
import life.knowledge4.videotrimmer.interfaces.OnK4LVideoListener;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;

import java.io.File;


public class VideoTrimmerActivity extends AppCompatActivity implements OnTrimVideoListener, OnK4LVideoListener, View.OnClickListener {

    private K4LVideoTrimmer mVideoTrimmer/*, mImageTrimmer*/;
    private TextView backBtn;
    private ProgressDialog mProgressDialog;
    String path = "";
    private Uri originalUri;
    private TextView trimTextView;
    private TextView coverTextView;
    private View trimLine;
    private View coverLine;
    private TextView done_crop_txt;
    public ImageView mPlayView;
    private int maxDuration = 180;
    private double mbPerSec;
    private Bundle bundle;
    private String hintname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trimmer);
        initViews();
    }


    protected void initViews() {
        backBtn = (TextView) findViewById(R.id.backBtn);
        mPlayView = ((ImageView) findViewById(R.id.icon_video_play));
        mVideoTrimmer = ((K4LVideoTrimmer) findViewById(R.id.videoTimeLine));
        //mImageTrimmer = ((K4LVideoTrimmer) findViewById(R.id.imageTimeLine));
        done_crop_txt = (TextView) findViewById(R.id.done_crop_txt);
        done_crop_txt.setOnClickListener(this);
        done_crop_txt.setText("NEXT");
        //Typeface custom_font_medium = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_medium));
        //done_crop_txt.setTypeface(custom_font_medium);
        trimTextView = (TextView) findViewById(R.id.trim_textview);
        coverTextView = (TextView) findViewById(R.id.cover_textview);
        trimTextView.setOnClickListener(this);
        coverTextView.setOnClickListener(this);
        trimLine = (View) findViewById(R.id.trim_view);
        coverLine = (View) findViewById(R.id.cover_view);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            path = bundle.getString("REAL_VIDEO_PATH");
            hintname = bundle.getString("hintname");
        }
        //setting progressbar
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Trimming Video..");

        backBtn.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           finish();
                                       }
                                   }
        );

        originalUri = Uri.parse(path);

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInmillisec = Long.parseLong(time);
        int duration = (int) (timeInmillisec / 1000);
        try {
            File file = new File(path);
            long length = file.length();
            length = length / 1024;
            if (length > 1024) {
                length = length / 1024;
                if (length > 30) {
                    mbPerSec = (float) length / (float) duration;
                    double size = 0;
                    int counter = 0;
                    while (size < 30) {
                        size = size + mbPerSec;
                        counter++;
                    }
                    if (size > 30) {
                        counter--;
                    }
                    maxDuration = counter;
                }
            } else {

            }
            System.out.println("File Path : " + file.getPath() + ", File size : " + length + " KB");
        } catch (Exception e) {
            System.out.println("File not found : " + e.getMessage() + e);
        }

        duration++;
        if (mVideoTrimmer != null) {
            mVideoTrimmer.setPlayView(mPlayView);
            mVideoTrimmer.setMaxDuration(maxDuration);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnK4LVideoListener(this);
            File rootPath = Environment.getExternalStorageDirectory();
            mVideoTrimmer.setDestinationPath(rootPath + "/Android/data/com.mpas.enlte/Videos/");
            mVideoTrimmer.setVideoURI(originalUri);
            mVideoTrimmer.setVideoInformationVisibility(true);
        }
       /* if (mImageTrimmer != null) {
            mImageTrimmer.setPlayView(mPlayView);
            mImageTrimmer.setMaxDuration(duration);
            mImageTrimmer.setOnTrimVideoListener(this);
            mImageTrimmer.setOnK4LVideoListener(this);
            File rootPath = Environment.getExternalStorageDirectory();
            mImageTrimmer.setDestinationPath(rootPath + "/Android/data/com.Android.Ten/Videos/");
            originalUri = Uri.parse(path);
            mImageTrimmer.setVideoURI(originalUri);
            mImageTrimmer.setVideoInformationVisibility(false);
            mImageTrimmer.setRangeBarVisibility(true);
            //mImageTrimmer.setScrollIndicators();
        }*/

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onTrimStarted() {
        mProgressDialog.show();
    }

    @Override
    public void getResult(final Uri croppedVideoUri, final int currentPosition) {
        mProgressDialog.cancel();

      /*  MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(VideoTrimmerActivity.this, originalUri);
        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(currentPosition, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        Log.d("","===>"+bitmap);*/
        /*
        finish();*/
        if (croppedVideoUri != null) {
            File videoFile = new File(croppedVideoUri.toString());
            String videoFilePath = videoFile.getPath();
            /*
            Intent intent = new Intent(VideoTrimmerActivity.this, PostDescriptionActivity.class);
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putString("media_path", videoFilePath);
            bundle.putString("hintname", hintname);
            intent.putExtras(bundle);
            startActivity(intent);*/
            Intent intent = new Intent();//Intent.ACTION_VIEW, croppedVideoUri
            intent.setDataAndType(croppedVideoUri, "video/mp4");
            intent.putExtra("position", currentPosition);
            intent.putExtra("videoFilePath",videoFilePath);
            setResult(RESULT_OK, intent);
            finish();
        }
    }


    @Override
    public void cancelAction() {
        mProgressDialog.cancel();
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(final String message) {
        mProgressDialog.cancel();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(VideoTrimmerActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVideoPrepared() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //  Toast.makeText(VideoTrimmerActivity.this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trim_textview:
                mVideoTrimmer.setVideoTrimView();
                mVideoTrimmer.setVideoInformationVisibility(true);
                //mVideoTrimmer.setVisibility(View.VISIBLE);
                //mImageTrimmer.setVisibility(View.GONE);
                trimLine.setBackgroundColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.greyColor));
                coverLine.setBackgroundColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.lightGreyColor));
                trimTextView.setTextColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.blackColor));
                coverTextView.setTextColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.greyColor));

                break;
            case R.id.cover_textview:
                /*mImageTrimmer.setCoverTrimView();
                mImageTrimmer.setVisibility(View.VISIBLE);
                mVideoTrimmer.setVisibility(View.GONE);*/
                mVideoTrimmer.setCoverTrimView();
                mVideoTrimmer.setVideoInformationVisibility(false);
                trimLine.setBackgroundColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.lightGreyColor));
                coverLine.setBackgroundColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.greyColor));
                trimTextView.setTextColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.greyColor));
                coverTextView.setTextColor(ContextCompat.getColor(VideoTrimmerActivity.this, R.color.blackColor));

                break;
            case R.id.done_crop_txt:
                mVideoTrimmer.onSaveClicked();
                break;
        }
    }

}
