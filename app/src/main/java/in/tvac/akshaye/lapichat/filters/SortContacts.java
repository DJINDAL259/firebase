package in.tvac.akshaye.lapichat.filters;

import in.tvac.akshaye.lapichat.data.local.contacts.Contact;

import java.util.Comparator;

public class SortContacts implements Comparator<Contact> {
    @Override
    public int compare(Contact e1, Contact e2) {
        return e1.getName().compareTo(e2.getName());
    }
}
