package in.tvac.akshaye.lapichat.data.model;

public class ReadMoreData {

    private boolean readMore = true;
    private boolean showTrimExpandedText = false;
    private int lineEndIndex = 0;
    private CharSequence trimCollapsedText = "";
    private CharSequence trimExpandedText = "";

    private boolean isDataSet = false;

    public boolean isReadMore() {
        return readMore;
    }

    public void setReadMore(boolean readMore) {
        this.readMore = readMore;
    }

    public boolean isShowTrimExpandedText() {
        return showTrimExpandedText;
    }

    public void setShowTrimExpandedText(boolean showTrimExpandedText) {
        this.showTrimExpandedText = showTrimExpandedText;
    }



    public CharSequence getTrimCollapsedText() {
        return trimCollapsedText;
    }

    public void setTrimCollapsedText(CharSequence trimCollapsedText) {
        this.trimCollapsedText = trimCollapsedText;
    }

    public CharSequence getTrimExpandedText() {
        return trimExpandedText;
    }

    public void setTrimExpandedText(CharSequence trimExpandedText) {
        this.trimExpandedText = trimExpandedText;
    }

    public boolean isDataSet() {
        return isDataSet;
    }

    public void setDataSet(boolean dataSet) {
        isDataSet = dataSet;
    }


    public int getLineEndIndex() {
        return lineEndIndex;
    }

    public void setLineEndIndex(int lineEndIndex) {
        this.lineEndIndex = lineEndIndex;
    }
}
