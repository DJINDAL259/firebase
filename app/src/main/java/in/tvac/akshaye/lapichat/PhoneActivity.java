package in.tvac.akshaye.lapichat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import in.tvac.akshaye.lapichat.views.auth.VerifyPhoneActivity;

public class PhoneActivity extends AppCompatActivity {

    private EditText editTextMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        editTextMobile = findViewById(R.id.editTextMobile);

        Button buttonContinue = findViewById(R.id.buttonContinue);
        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateField();
            }
        });
    }

    private void validateField() {
        String number = editTextMobile.getText().toString().trim();
        if (number.length() < 10) {
            editTextMobile.setError("Invalid phone number");
            editTextMobile.requestFocus();
            return;
        }
        String prefix = "+91";
        number = prefix + number;

        Intent intent = new Intent(PhoneActivity.this, VerifyPhoneActivity.class);
        intent.putExtra("mobile", number);
        startActivity(intent);
    }
}
