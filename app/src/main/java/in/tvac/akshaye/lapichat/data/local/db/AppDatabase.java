/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package in.tvac.akshaye.lapichat.data.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import in.tvac.akshaye.lapichat.data.local.db.dao.PostDao;
import in.tvac.akshaye.lapichat.data.local.db.dao.UserDao;
import in.tvac.akshaye.lapichat.data.model.api.BlinkTimeLineResponse;
import in.tvac.akshaye.lapichat.data.model.db.User;


/**
 * Created  on 07/07/17.
 */

@Database(entities = {User.class, BlinkTimeLineResponse.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    private static final String DB_NAME = "lapichat_Database.db";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DB_NAME).allowMainThreadQueries().build();
    }

    public abstract PostDao postDao();

    public abstract UserDao userDao();
}
