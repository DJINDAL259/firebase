package in.tvac.akshaye.lapichat.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import in.tvac.akshaye.lapichat.data.model.api.BlinkTimeLineResponse;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface PostDao {

    @Insert
    void saveNewPost(ArrayList<BlinkTimeLineResponse> postData);

    @Query("SELECT * FROM timelines")
     List<BlinkTimeLineResponse> getAllPost();
}
