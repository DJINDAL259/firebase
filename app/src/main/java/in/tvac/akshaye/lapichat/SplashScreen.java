package in.tvac.akshaye.lapichat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Window;
import android.widget.TextView;
import in.tvac.akshaye.lapichat.data.local.pref.AppPreferencesHelper;
import in.tvac.akshaye.lapichat.utils.GlobalConstants;
import in.tvac.akshaye.lapichat.views.auth.InviteFriendActivity;
import in.tvac.akshaye.lapichat.views.auth.RegistrationActivity;


/**
 * Created by mpas on 9/1/2017.
 */

public class SplashScreen extends AppCompatActivity {

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        String appVersion = "1.0";
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        preferences = AppPreferencesHelper.getPref(this);

        TextView app_version_text = findViewById(R.id.app_version_text);
        app_version_text.setText("Version: v_" + appVersion);

        int secondsDelayed = 3;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //openDialog();
                openNextScreen();
            }
        }, secondsDelayed * 1000);

    }


    private void openNextScreen() {
        String userId = preferences.getString(GlobalConstants.USER_ID, "");
        if (!TextUtils.isEmpty(userId)) {
            LapichatApp.userId = userId;
            startActivity(new Intent(SplashScreen.this, InviteFriendActivity.class));
        } else {
            startActivity(new Intent(SplashScreen.this, RegistrationActivity.class));
        }
        //}
        finish();
    }

}
