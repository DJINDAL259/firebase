package in.tvac.akshaye.lapichat.data.local.contacts;

public class ContactEmail {
	public String address;
	public String type;

	public ContactEmail(String address, String type) {
		this.address = address;
		this.type = type;
	}
}
