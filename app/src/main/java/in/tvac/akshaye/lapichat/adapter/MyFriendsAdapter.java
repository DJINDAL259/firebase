package in.tvac.akshaye.lapichat.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.contacts.Contact;
import in.tvac.akshaye.lapichat.utils.Utils;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Administrator on 9/15/2017.
 */

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.MyViewHolder> {

    private ArrayList<Contact> contactArrayList;
    Context context;

    public MyFriendsAdapter(ArrayList<Contact> feedList, Context ctx) {
        this.contactArrayList = feedList;
        this.context = ctx;
    }

    public void updateList(ArrayList<Contact> contactList) {
        this.contactArrayList = contactList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView userNameTV, phoneTV, emailTV;
        public ImageView userImage;
        private TextView button;

        public MyViewHolder(View view) {
            super(view);
            //rowCounter =  view.findViewById(R.id.rowCounter);
            userImage = view.findViewById(R.id.user_image);
            userNameTV = view.findViewById(R.id.tvName);
            phoneTV = view.findViewById(R.id.tvPhone);
            emailTV = view.findViewById(R.id.tvEmail);
            button = view.findViewById(R.id.invite_btn);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contactArrayList.get(getLayoutPosition()).isExist) {
                        Toast.makeText(context, "Coming soon", Toast.LENGTH_LONG).show();
                    } else {
                        Utils.inviteFriends((Activity) context, "iLookh");
                    }
                }
            });
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact_friends, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Contact contact = contactArrayList.get(position);
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

     //holder.userImage.setBackgroundColor(color);
        holder.userImage.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        //holder.userImage.setBackgroundTintList(ContextCompat.get(context, color));

        holder.userNameTV.setText(contact.name);
        holder.emailTV.setText("");
        holder.phoneTV.setText("");
        if (contact.emails.size() > 0 && contact.emails.get(0) != null) {
            holder.emailTV.setText(contact.emails.get(0).address);
        }
        if (contact.numbers.size() > 0 && contact.numbers.get(0) != null) {
            holder.phoneTV.setText(contact.numbers.get(0).number);
            //retrieveContactPhoto(context, contact.numbers.get(0).number, holder.userImage);
        }
        //Log.d("onScrolled",  "===" + position);
       // holder.button.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        if (contact.isExist) {
            holder.button.setText("Add");
         //   holder.button.setTextColor(ContextCompat.getColor(context, R.color.green_color));
        } else {
            holder.button.setText("Invite");
        }
    }

    @Override
    public int getItemCount() {
        return (contactArrayList.size() > 0 ? contactArrayList.size() : 0);
    }

    /*public Bitmap retrieveContactPhoto(Context context, String number, ImageView imageView) {
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_circular);

        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId));
            Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);

            AssetFileDescriptor fd = context.getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
            InputStream inputStream = fd.createInputStream();


            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            // e.printStackTrace();
        }
        if (photo != null) {
            imageView.setImageBitmap(photo);
        }
        return photo;
    }*/
}


