package in.tvac.akshaye.lapichat.data.model.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OutputAddress {

    @SerializedName("users")
    @Expose
    private List<User> users = null;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public class User {

        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("isSingedUp")
        @Expose
        private Boolean isSingedUp;

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Boolean getIsSingedUp() {
            return isSingedUp;
        }

        public void setIsSingedUp(Boolean isSingedUp) {
            this.isSingedUp = isSingedUp;
        }

    }
}
