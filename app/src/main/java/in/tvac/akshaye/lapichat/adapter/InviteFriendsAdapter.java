package in.tvac.akshaye.lapichat.adapter;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.*;
import de.hdodenhof.circleimageview.CircleImageView;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.contacts.Contact;
import in.tvac.akshaye.lapichat.utils.Utils;
import in.tvac.akshaye.lapichat.views.auth.InviteFriendActivity;
import in.tvac.akshaye.lapichat.views.auth.VerifyOTPActivity;
import in.tvac.akshaye.lapichat.views.profile.AddFriendActivity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Administrator on 9/15/2017.
 */

public class InviteFriendsAdapter extends RecyclerView.Adapter<InviteFriendsAdapter.MyViewHolder> {

    private final DatabaseReference databaseRef;
    private ArrayList<Contact> contactArrayList;
    Context context;

    public InviteFriendsAdapter(ArrayList<Contact> feedList, Context ctx) {
        this.contactArrayList = feedList;
        this.context = ctx;
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        databaseRef = database.getReference();
    }

    public void updateList(ArrayList<Contact> contactList) {
        this.contactArrayList = contactList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView userNameTV, phoneTV, emailTV;
        public ImageView userImage;
        private TextView button;

        public MyViewHolder(View view) {
            super(view);
            //rowCounter =  view.findViewById(R.id.rowCounter);
            userImage = view.findViewById(R.id.user_image);
            userNameTV = view.findViewById(R.id.tvName);
            phoneTV = view.findViewById(R.id.tvPhone);
            emailTV = view.findViewById(R.id.tvEmail);
            button = view.findViewById(R.id.invite_btn);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contactArrayList.get(getLayoutPosition()).isExist) {
                        button.setEnabled(false);
                        //Toast.makeText(context, "Coming soon", Toast.LENGTH_LONG).show();
                        Contact contact = contactArrayList.get(getLayoutPosition());
                        if (contact.numbers.size() > 0 && contact.numbers.get(0) != null) {
                            String number = contact.numbers.get(0).number;

                            Query query = databaseRef.child("users").orderByChild("mobile").equalTo(number);
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    button.setEnabled(true);
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        // 1 or more users exist which have the username property "usernameToCheckIfExists"

                                        for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                            Log.d("-2-", "" + dataSnapshot2.getRef().getKey());
                                            String userId = dataSnapshot2.getRef().getKey();
                                            goToNext(userId);
                                            //goToNext(dataSnapshot2.getRef().getKey());
                                        }
                                        //dataSnapshot.getRef().getKey();
                                    } else {
                                        //newSignUp(database);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.d("--", "" + databaseError.toString());
                                    //progressBar.setVisibility(View.GONE);
                                    button.setEnabled(true);
                                }
                            });
                        }

                    } else {
                        Utils.inviteFriends((Activity) context, "iLookh");
                    }
                }
            });
        }

        private void goToNext(String userId) {
            Intent intent = new Intent(context, AddFriendActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("user_id", userId);
            context.startActivity(intent);
            //finish();
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_contact_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Contact contact = contactArrayList.get(position);
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        //holder.userImage.setBackgroundColor(color);
        holder.userImage.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        //holder.userImage.setBackgroundTintList(ContextCompat.get(context, color));

        holder.userNameTV.setText(contact.name);
        holder.emailTV.setText("");
        holder.phoneTV.setText("");
        if (contact.emails.size() > 0 && contact.emails.get(0) != null) {
            holder.emailTV.setText(contact.emails.get(0).address);
        }
        if (contact.numbers.size() > 0 && contact.numbers.get(0) != null) {
            holder.phoneTV.setText(contact.numbers.get(0).number);
            //retrieveContactPhoto(context, contact.numbers.get(0).number, holder.userImage);
        }
        //Log.d("onScrolled",  "===" + position);
        // holder.button.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        if (contact.isExist) {
            holder.button.setText("Add");
            //   holder.button.setTextColor(ContextCompat.getColor(context, R.color.green_color));
        } else {
            holder.button.setText("Invite");
        }
    }

    @Override
    public int getItemCount() {
        return (contactArrayList.size() > 0 ? contactArrayList.size() : 0);
    }

    /*public Bitmap retrieveContactPhoto(Context context, String number, ImageView imageView) {
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_circular);

        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId));
            Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);

            AssetFileDescriptor fd = context.getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
            InputStream inputStream = fd.createInputStream();


            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            // e.printStackTrace();
        }
        if (photo != null) {
            imageView.setImageBitmap(photo);
        }
        return photo;
    }*/
}


