package in.tvac.akshaye.lapichat.views.auth;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.databinding.ActivityDatePickerBinding;
import in.tvac.akshaye.lapichat.views.BaseActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePickerActivity extends BaseActivity implements View.OnClickListener {

    ActivityDatePickerBinding binding;
    //private int mYear, mDay, mMonth;
    //private Bundle bundle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_date_picker;
    }

    @Override
    protected void initView() {
        binding = (ActivityDatePickerBinding) viewDataBinding;

        binding.btnDone.setOnClickListener(this::onClick);
        //bundle = getIntent().getExtras();
        binding.editTextDobYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (data.length() == 4) {
                    binding.editTextDobMonth.requestFocus();
                }
            }
        });
        binding.editTextDobMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (data.length() == 2) {
                    binding.editTextDobDay.requestFocus();
                }
            }
        });
        binding.editTextDobDay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (data.length() == 2) {
                    hideKeyboard();
                }
            }
        });
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        final Calendar c = Calendar.getInstance();

        switch (v.getId()) {

            case R.id.btn_done:
                String yearStr = binding.editTextDobYear.getText().toString().trim();
                String monthStr = binding.editTextDobMonth.getText().toString().trim();
                String dayStr = binding.editTextDobDay.getText().toString().trim();

                if (TextUtils.isEmpty(yearStr)) {
                    showToast("Please enter the year of birth");
                } else if (TextUtils.isEmpty(monthStr)) {
                    showToast("Please enter the month of birth");
                } else if (TextUtils.isEmpty(dayStr)) {
                    showToast("Please enter the day of birth");
                } else {
                    int year = Integer.parseInt(yearStr);
                    int month = Integer.parseInt(monthStr);
                    int day = Integer.parseInt(dayStr);

                    int minimumAge = c.get(Calendar.YEAR) - 14;//15
                    int maxAge = c.get(Calendar.YEAR) - 100;//100

                    if (year > minimumAge) {
                        showToast("Mimimum age limit is 15 years");
                    } else if (year < maxAge) {
                        showToast("Maximum age limit is 100 years");
                    }
                    if (month < 1 || month > 12) {
                        showToast("Please enter valid month");
                    } else if (day < 1 || day > 31) {
                        showToast("Please enter valid day");
                    } else {
                        int age = getAge(year, month, day);
                        if (age < 15) {
                            showToast("Mimimum age limit is 15 years");
                        } else {
                            if (month < 10 && !monthStr.contains("0")) {
                                monthStr = "0" + monthStr;
                            }
                            if (day < 10 && !dayStr.contains("0")) {
                                dayStr = "0" + dayStr;
                            }
                            String dob = dayStr + "/" + monthStr + "/" + yearStr;

                            if (validateJavaDate(dob)) {

                                Intent intent = new Intent();
                                intent.putExtra("dob", dob);
                                setResult(RESULT_OK, intent);
                                finish();
                            } else {
                                showToast("Invalid date");
                            }
                        }
                    }
                }
                break;

            case R.id.edit_text_dob:
                // Get Current Date


                break;
        }
    }

    public int getAge(int DOByear, int DOBmonth, int DOBday) {

        int age;

        final Calendar calenderToday = Calendar.getInstance();
        int currentYear = calenderToday.get(Calendar.YEAR);
        int currentMonth = 1 + calenderToday.get(Calendar.MONTH);
        int todayDay = calenderToday.get(Calendar.DAY_OF_MONTH);

        age = currentYear - DOByear;

        if (DOBmonth > currentMonth) {
            --age;
        } else if (DOBmonth == currentMonth) {
            if (DOBday > todayDay) {
                --age;
            }
        }
        return age;
    }


    public static boolean validateJavaDate(String strDate) {
        /* Check if date is 'null' */
        if (strDate.trim().equals("")) {
            return true;
        }
        /* Date is not 'null' */
        else {
            /*
             * Set preferred date format,
             * For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.*/
            SimpleDateFormat sdfrmt = new SimpleDateFormat("dd/MM/yyyy");
            sdfrmt.setLenient(false);
            /* Create Date object
             * parse the string into date
             */
            try {
                Date javaDate = sdfrmt.parse(strDate);
                System.out.println(strDate + " is valid date format");
            }
            /* Date format is invalid */ catch (ParseException e) {
                System.out.println(strDate + " is Invalid Date format");
                return false;
            }
            /* Return true if date format is valid */
            return true;
        }
    }
}
