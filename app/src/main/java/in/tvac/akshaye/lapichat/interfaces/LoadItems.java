package in.tvac.akshaye.lapichat.interfaces;

public interface LoadItems {
    public void loadMore(String nodeId, int count);

    public void showDialog();

    public void hideDialog();
}
