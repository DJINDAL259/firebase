package in.tvac.akshaye.lapichat.data.model.api;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "timelines")
public class BlinkTimeLineResponse {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private String id;

    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "postImage")
    private String postImage;

    @ColumnInfo(name = "video_url")
    private String video_url;

    @ColumnInfo(name = "auth_message")
    private String auth_message;

    @ColumnInfo(name = "caption")
    private String caption;


    @ColumnInfo(name = "username")
    private String username;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getAuth_message() {
        return auth_message;
    }

    public void setAuth_message(String auth_message) {
        this.auth_message = auth_message;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
