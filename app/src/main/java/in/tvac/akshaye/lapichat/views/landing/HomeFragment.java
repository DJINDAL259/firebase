package in.tvac.akshaye.lapichat.views.landing;

import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.google.firebase.database.*;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.adapter.TimeLineAdapter;
import in.tvac.akshaye.lapichat.data.local.db.AppDatabase;
import in.tvac.akshaye.lapichat.data.model.api.BlinkTimeLineResponse;
import in.tvac.akshaye.lapichat.databinding.FragmentHomeBinding;
import in.tvac.akshaye.lapichat.interfaces.LoadItems;
import in.tvac.akshaye.lapichat.views.BaseFragment;

import java.util.ArrayList;

import static in.tvac.akshaye.lapichat.LapichatApp.userId;

public class HomeFragment extends BaseFragment implements LoadItems {
    private FragmentHomeBinding binding;
    private TimeLineAdapter mAdapter;
    private ArrayList<BlinkTimeLineResponse> mTimelineList = new ArrayList<>();
    private AppDatabase db;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViews(View view) {
        binding = (FragmentHomeBinding) viewDataBinding;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recyclerViewHome.setLayoutManager(linearLayoutManager);
        db = AppDatabase.getInstance(mContext);
        new Thread(() -> {
            mTimelineList = (ArrayList<BlinkTimeLineResponse>) db.postDao().getAllPost();
            getBaseActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mTimelineList.size() > 0) {
                        setAdapter();
                        hideDialog();
                        fetchData("", mTimelineList.size());
                    } else {
                        fetchData("", 10);
                        showDialog();
                    }
                }
            });

        }).start();
    }

    private void fetchData(String nodeId, int limit) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("Timeline").child(userId);
        Query query;
        if (TextUtils.isEmpty(nodeId)) {
            query = myRef.limitToFirst(limit);
        } else {
            query = myRef.orderByKey().startAt(nodeId).limitToFirst(limit);
            showDialog();
        }

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideDialog();
                if (dataSnapshot.exists()) {
                    final ArrayList<BlinkTimeLineResponse> timelineList = new ArrayList<>();

                    int count = (int) dataSnapshot.getChildrenCount();


                    for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                        BlinkTimeLineResponse blinkTimeLineResponse = dataSnapshot2.getValue(BlinkTimeLineResponse.class);

                        if (mTimelineList.size() > 0) {
                            boolean isKeyFound = false;
                            for (int i = 0; i < mTimelineList.size(); i++) {
                                if (dataSnapshot2.getKey().equals(mTimelineList.get(i).getId())) {
                                    isKeyFound = true;
                                    break;
                                }
                            }
                            if (!isKeyFound) {
                                blinkTimeLineResponse.setId(dataSnapshot2.getKey());
                                timelineList.add(blinkTimeLineResponse);
                                mTimelineList.add(blinkTimeLineResponse);
                            }

                        } else {
                            blinkTimeLineResponse.setId(dataSnapshot2.getKey());
                            timelineList.add(blinkTimeLineResponse);
                            mTimelineList.add(blinkTimeLineResponse);
                        }

                    }
                    //}
                    if (timelineList.size() > 0) {
                        new Thread(() -> db.postDao().saveNewPost(timelineList)).start();

                        if (mAdapter == null) setAdapter();
                        else {
                            mAdapter.submitList(mTimelineList);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    Log.e(TAG, "Test");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                hideDialog();

            }
        });
    }


    private void setAdapter() {
        mAdapter = new TimeLineAdapter(mTimelineList, this);
        binding.recyclerViewHome.setAdapter(mAdapter);
        mAdapter.submitList(mTimelineList);
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void loadMore(String nodeId, int count) {
        fetchData(nodeId, count);
    }
}
