package in.tvac.akshaye.lapichat.views.landing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import android.view.View;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.data.local.pref.AppPreferencesHelper;
import in.tvac.akshaye.lapichat.databinding.ActivityLandingBinding;
import in.tvac.akshaye.lapichat.utils.GlobalConstants;
import in.tvac.akshaye.lapichat.views.BaseActivity;
import in.tvac.akshaye.lapichat.views.auth.RegistrationActivity;

public class LandingActivity extends BaseActivity {

    private ActivityLandingBinding binding;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_landing;
    }

    @Override
    protected void initView() {
        binding = (ActivityLandingBinding) viewDataBinding;

        SharedPreferences preferences = AppPreferencesHelper.getPref(LandingActivity.this);
        String userId = preferences.getString(GlobalConstants.USER_ID, "");
        if (userId != null && userId.length() > 0) {
            //LapichatApp.userId = userId;
        }

        binding.logoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = AppPreferencesHelper.getPref(LandingActivity.this);
                preferences.edit().putString(GlobalConstants.USER_MOBILE_ID, "").commit();
                preferences.edit().putString(GlobalConstants.USER_ID, "").commit();
                //verification successful we will start the profile activity
                Intent intent = new Intent(LandingActivity.this, RegistrationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        binding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        replaceFragment(binding.frameLayout, new HomeFragment(), null);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (getFragment() instanceof HomeFragment) return true;
                    replaceFragment(binding.frameLayout, new HomeFragment(), null);
                    return true;
                case R.id.navigation_add_post:
                    if (getFragment() instanceof AddPostFragment) return true;
                    replaceFragment(binding.frameLayout, new AddPostFragment(), null);
                    return true;
                case R.id.navigation_profile:
                    if (getFragment() instanceof ProfileFragment) return true;
                    replaceFragment(binding.frameLayout, new ProfileFragment(), null);
                    return true;
                case R.id.navigation_notifications:
                    return true;
            }
            return false;
        }
    };

   /* public void addFragment(FrameLayout frameLayout, Fragment newFragment, Bundle args) {
        FragmentManager manager = getSupportFragmentManager();
        //manager.popBackStack();
        if (args != null)
            newFragment.setArguments(args);
        FragmentTransaction transaction = manager.beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.add(frameLayout.getId(), newFragment);
     //   transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        int count = getSupportFragmentManager().getBackStackEntryCount();
        Log.e("count--", count + "");
        //  finish();
    }
}
