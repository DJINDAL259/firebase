package in.tvac.akshaye.lapichat.views.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import com.hbb20.CountryCodePicker;
import in.tvac.akshaye.lapichat.R;
import in.tvac.akshaye.lapichat.databinding.ActivityRegistrationBinding;
import in.tvac.akshaye.lapichat.views.BaseActivity;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener {
    private static final int DATE_PICKER = 555;
    private CountryCodePicker ccp;
    private ActivityRegistrationBinding binding;
    //private int mYear, mDay, mMonth;
    //private FirebaseAuth mAuth;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_registration;
    }

    @Override
    protected void initView() {
        binding = (ActivityRegistrationBinding) viewDataBinding;
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        binding.editTextDob.setOnClickListener(this);
        binding.btnNext.setOnClickListener(this);

        ccp.setCountryForPhoneCode(91);

        //mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                checkValidations();
                break;
            case R.id.edit_text_dob:
                // Get Current Date
                /*final Calendar c = Calendar.getInstance();

                if (TextUtils.isEmpty(binding.editTextDob.getText())) {

                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    String date = binding.editTextDob.getText().toString();
                    StringTokenizer stringTokenizer = new StringTokenizer(date, "/");
                    mDay = Integer.parseInt(stringTokenizer.nextToken());
                    mMonth = Integer.parseInt(stringTokenizer.nextToken());
                    mYear = Integer.parseInt(stringTokenizer.nextToken());
                    mMonth--;
                }

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    new SpinnerDatePickerDialogBuilder()
                            .context(RegistrationActivity.this)
                            .callback(RegistrationActivity.this)
                            .spinnerTheme(R.style.NumberPickerStyle)
                            .showTitle(true)
                            .showDaySpinner(true)
                            .defaultDate(mYear, mMonth, mDay)
                            .maxDate(c.get(Calendar.YEAR) - 14, 11, 31)
                            .minDate(c.get(Calendar.YEAR) - 100, 0, 1)
                            .build()
                            .show();

                } else {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            (view, year, monthOfYear, dayOfMonth) -> binding.editTextDob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                    datePickerDialog.show();
                }*/

                Intent intent = new Intent(RegistrationActivity.this, DatePickerActivity.class);
                startActivityForResult(intent, DATE_PICKER);

                /**/
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && requestCode == DATE_PICKER) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String dob = bundle.getString("dob");
                binding.editTextDob.setText(dob);
            }
        }
    }


    /* @Override
    public void onDateSet(in.tvac.akshaye.lapichat.views.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        binding.editTextDob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
    }*/


    private void checkValidations() {

        String name = binding.editTextUsername.getText().toString().trim();
        String dob = binding.editTextDob.getText().toString().trim();
        String number = binding.edtTxtNumber.getText().toString().trim();
        String password = binding.editTextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(binding.editTextUsername.getText().toString().trim())) {
            showToast("Please enter username");
        } else if (TextUtils.isEmpty(binding.edtTxtNumber.getText().toString().trim())) {
            showToast("Please enter number");
        } else if (number.length() < 10) {
            binding.edtTxtNumber.setError("Invalid phone number");
            binding.edtTxtNumber.requestFocus();
            return;
        } else if (TextUtils.isEmpty(binding.editTextDob.getText().toString().trim())) {
            showToast("Please enter date of birth");
        } else if (TextUtils.isEmpty(binding.editTextPassword.getText().toString().trim())) {
            showToast("Please enter password");
        } else if (binding.editTextPassword.getText().toString().trim().length() < 6) {
            showToast("Password minimum length is 6");
        } else {

            String prefix = ccp.getSelectedCountryCode();
            number = "+" + prefix + number;

            Intent intent = new Intent(this, VerifyOTPActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("mobile", number);
            intent.putExtra("dob", dob);
            intent.putExtra("password", password);

            startActivity(intent);
        }
    }


}
