package com.flashboys.ilmio.utils;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtils {

    //fonts/SansPro/SourceSansPro-Regular.ttf

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
    }

    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Bold.ttf");
    }

    public static Typeface getExtraBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-ExtraBold.ttf");
    }

    public static Typeface getMediumFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
    }

    public static Typeface getSemiBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-SemiBold.ttf");
    }

    public static Typeface getThinFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
    }

    public static Typeface getItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
    }
}
